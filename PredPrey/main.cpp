#include <iostream>

#include "World.h"
#include "Constants.h"
#include <cstdio>
#include <ctime>
#include <sstream>
#include <iomanip>
#include <chrono>
#include <ctime>


int runNumberOfRLExperiments(int start, int stop);
int runNumberOfPTLExperiments(int start, int stop);
void evaluateBranchPerformance(std::string runNumberFormatted, int testTime);

int main()
{
  std::cout << "Project " << EXP_NAME() << "\n";
  std::chrono::time_point<std::chrono::system_clock> start, end;
  start = std::chrono::system_clock::now();
  int runNumber = START_NUMBER_OF_EXPERIMENT;
  std::stringstream ss;
  ss << "mkdir \"" << OUT_PATH();
  system(ss.str().c_str());
  runNumberOfPTLExperiments(runNumber, (NUMBER_OF_RUNS + START_NUMBER_OF_EXPERIMENT));

  ss.str("");
  ss << "cmd /c " << BAT_PATH() << "moveFiles.bat " << OUT_PATH() << " " << EXP_NAME() << " stats";
  //dont need bat anymore?//system(ss.str().c_str());
  end = std::chrono::system_clock::now();
  std::chrono::duration<double> elapsed_seconds = end - start;
  std::time_t end_time = std::chrono::system_clock::to_time_t(end);
  std::cout << "finished computation at " << std::ctime(&end_time) << "elapsed time: " << elapsed_seconds.count() << "s\n";
  std::cout << "Ended " << EXP_NAME() << "\n";
  std::cin.get();
}

int runNumberOfRLExperiments(int start, int stop)
{
  for (int internalRun = start; internalRun < stop; internalRun++)
    {
      std::stringstream ss;
      ss << std::setfill('0') << std::setw(4) << internalRun;
      std::string runNumberString = ss.str();
      std::cout << "current exp is " << internalRun << " of " << stop << "\n";
      std::shared_ptr<World> w(std::make_shared<World>());

      ss.str("");
      ss << EXP_NAME() << "-a";
      std::shared_ptr<Preditor> pred1(std::make_shared<Preditor>(ss.str(), WORLD_WIDTH, WORLD_HIGHT, 1));
      ss.str("");
      ss << EXP_NAME() << "-b";
      pred1->SetPosition(std::pair<int, int>(2, 3));
      std::shared_ptr<Preditor> pred2(std::make_shared<Preditor>(ss.str(), WORLD_WIDTH, WORLD_HIGHT, 2));
      ss.str("");
      pred2->SetPosition(std::pair<int, int>(2, 4));
      /*ss << EXP_NAME() << "-c";
      Preditor* pred3 = new Preditor(WORLD_WIDTH, WORLD_HIGHT, 3);
      pred3->SetLocalName(ss.str());
      ss.str("");
      ss << EXP_NAME() << "-d";
      pred3->SetPosition(std::pair<int, int>(6, 6));
      Preditor* pred4 = new Preditor(WORLD_WIDTH, WORLD_HIGHT, 4);
      pred4->SetLocalName(ss.str());
      ss.str("");
      ss << EXP_NAME() << "-e";
      pred4->SetPosition(std::pair<int, int>(1, 1));
      Preditor* pred5 = new Preditor(WORLD_WIDTH, WORLD_HIGHT, 5);
      pred5->SetLocalName(ss.str());
      ss.str("");
      ss << EXP_NAME() << "-f";
      pred5->SetPosition(std::pair<int, int>(2, 2));*/


      std::shared_ptr<Prey> prey1 = (std::make_shared<Prey>());
      prey1->SetUsingLines(true);
      prey1->SetLocalName("A");
      prey1->SetPosition(std::pair<int, int>(3, 2));
      std::shared_ptr<Prey> prey2 = (std::make_shared<Prey>());
      prey2->SetUsingLines(true);
      prey2->SetLocalName("B");
      prey2->SetPosition(std::pair<int, int>(3, 3));
      std::shared_ptr<Prey> prey3 = (std::make_shared<Prey>());
      prey3->SetUsingLines(true);
      prey3->SetLocalName("C");
      prey3->SetPosition(std::pair<int, int>(3, 4));
      std::shared_ptr<Prey> prey4 = (std::make_shared<Prey>());
      prey4->SetLocalName("D");
      prey4->SetPosition(std::pair<int, int>(6, 2));
      std::shared_ptr<Prey> prey5 = (std::make_shared<Prey>());
      prey5->SetLocalName("E");
      prey5->SetPosition(std::pair<int, int>(6, 1));
      std::shared_ptr<Prey> prey6 = (std::make_shared<Prey>());
      prey6->SetLocalName("F");
      prey6->SetPosition(std::pair<int, int>(1, 1));
      std::shared_ptr<Prey> prey7 = (std::make_shared<Prey>());
      prey7->SetLocalName("G");
      prey7->SetPosition(std::pair<int, int>(1, 4));
      std::shared_ptr<Prey> prey8 = (std::make_shared<Prey>());
      prey8->SetLocalName("H");
      prey8->SetPosition(std::pair<int, int>(2, 4));

      w->addPreditor(pred1);
      w->addPreditor(pred2);
      /*w->addPreditor(pred3);
      w->addPreditor(pred4);
      w->addPreditor(pred5);*/

      w->addPrey(prey1);
      /*w->addPrey(prey2);
      w->addPrey(prey3);
      w->addPrey(prey4);
      w->addPrey(prey5);
      w->addPrey(prey6);
      w->addPrey(prey7);
      w->addPrey(prey8);*/
      //w->printToConsole();

      //std::cout << w->serialiseWorld();
      ss.str("");
      ss << OUT_PATH() << runNumberString << "_output" << "_" << EXP_NAME() << ".ppstream";
      w->printToFile("", false, ss.str());
      ss.str("");
      for (int a = 0; a < EXPERIMENT_LENGTH; a++)
        {
          if (a == EXPLORE_LENGTH)
            {//finished learning
              w->setExploitaion();
            }
          w->printToFile(w->serialiseWorld());
          w->timeStep("RL", false);
        }
      w->printToFile("", true);
      /*ss << runNumberString << "_P1-EndOfRun";
      pred1->writeRLPolicies(ss.str(), "test");
      ss.str("");
      ss << runNumberString << "_P2-EndOfRun";
      pred2->writeRLPolicies(ss.str(), "test");
      ss.str("");
      ss << runNumberString << "_P3-EndOfRun";
      pred3->writeRLPolicies(ss.str(), "test");*/
      ss.str("");
      ss << runNumberString << "_EndOfRun";
      w->printTracesToFile(ss.str(), EXP_NAME());
    }

  /*std::stringstream ss;

  ss.str("");
  {
      ss << "cmd /c " << CURRENT_PATH() << "moveFiles.bat " << CURRENT_PATH() << " " << EXP_NAME() << " stats";
      system(ss.str().c_str());
  }*/
  return 1;
}

int runNumberOfPTLExperiments(int start, int stop)
{
  for (int internalRun = start; internalRun < stop; internalRun++)
    {
      std::stringstream ss;
      ss << std::setfill('0') << std::setw(4) << internalRun;
      std::string runNumberString = ss.str();
      std::cout << "++++++++current exp is " << internalRun << " of " << stop << "\n";
      std::shared_ptr<World> w = (std::make_shared<World>());
      std::shared_ptr<Preditor> pred1;
      std::shared_ptr<Preditor> pred2;
      std::shared_ptr<Preditor> pred3;
      std::shared_ptr<Preditor> pred4;
      if (NUMBER_OF_PREDITORS > 0)
        {
          ss.str("");
          ss << "a";
          pred1 = (std::make_shared<Preditor>(ss.str(), WORLD_WIDTH, WORLD_HIGHT, PREDITOR_VISION_RANGE));
          pred1->SetPosition(std::pair<int, int>(3, 3));
          pred1->publishRLStateSpace(OUT_PATH(), "plain");
        }
      if (NUMBER_OF_PREDITORS > 1)
        {
          ss.str("");
          ss << "b";
          pred2 = (std::make_shared<Preditor>(ss.str(), WORLD_WIDTH, WORLD_HIGHT, PREDITOR_VISION_RANGE));
          ss.str("");
          pred2->SetPosition(std::pair<int, int>(4, 4));
          pred2->publishRLStateSpace(OUT_PATH(), "plain");
        }
      if (NUMBER_OF_PREDITORS > 2)
        {

          ss.str("");
          ss << "c";
          pred3 = (std::make_shared<Preditor>(ss.str(), WORLD_WIDTH, WORLD_HIGHT, PREDITOR_VISION_RANGE));
          ss.str("");
          pred3->SetPosition(std::pair<int, int>(4, 3));
          pred3->publishRLStateSpace(OUT_PATH(), "plain");
        }
      if (NUMBER_OF_PREDITORS > 3)
        {
          ss.str("");
          ss << "d";
          pred4 = (std::make_shared<Preditor>(ss.str(), WORLD_WIDTH, WORLD_HIGHT, PREDITOR_VISION_RANGE));
          ss.str("");
          pred4->SetPosition(std::pair<int, int>(1, 3));
          pred4->publishRLStateSpace(OUT_PATH(), "plain");
        }
      //now make a mapping
      if (usingPTL)
        {
          if (USE_VISION_POLICY)
            {
              std::shared_ptr<QTable> pred1QTable = (std::make_shared<QTable>());
              std::vector<std::string> pred1Policies;
              std::shared_ptr<QTable> pred2QTable = (std::make_shared<QTable>());
              std::vector<std::string> pred2Policies;
              std::shared_ptr<QTable> pred3QTable = (std::make_shared<QTable>());
              std::vector<std::string> pred3Policies;
              std::shared_ptr<QTable> pred4QTable = (std::make_shared<QTable>());
              std::vector<std::string> pred4Policies;
              if (NUMBER_OF_PREDITORS > 0)
                {
                  pred1QTable->readStateActionFromFile(OUT_PATH() + "+" + pred1->GetLocalName() + "+Local+" + "PredVision" + "-q");
                  pred1Policies = pred1->getLocalPolicyNames();
                }
              if (NUMBER_OF_PREDITORS > 1)
                {
                  pred2QTable->readStateActionFromFile(OUT_PATH() + "+" + pred2->GetLocalName() + "+Local+" + "PredVision" + "-q");
                  pred2Policies = pred2->getLocalPolicyNames();
                  std::vector<std::string>::iterator pred1PoliciesIterator = pred1Policies.begin();
                  while (pred1PoliciesIterator != pred1Policies.end())
                    {
                      //can do this as we are the source and target we are mapping are the same policy
                      pred1->addPTLMapping((*pred1PoliciesIterator), (*pred1PoliciesIterator), pred1QTable, pred2QTable);
                      pred1PoliciesIterator++;
                    }
                  std::vector<std::string>::iterator pred2PoliciesIterator = pred2Policies.begin();
                  while (pred2PoliciesIterator != pred2Policies.end())
                    {
                      //can do this as we are the source and target we are mapping are the same policy
                      pred2->addPTLMapping((*pred2PoliciesIterator), (*pred2PoliciesIterator), pred2QTable, pred1QTable);
                      pred2PoliciesIterator++;
                    }
                  //pred2->addPTLMapping(pred1->GetLocalName(), pred2QTable, pred1QTable);
                  //pred1->addPTLMapping(pred2->GetLocalName(), pred1QTable, pred2QTable);
                }
              if (NUMBER_OF_PREDITORS > 2)
                {
                  pred3QTable->readStateActionFromFile(OUT_PATH() + "+" + pred3->GetLocalName() + "+Local+" + "PredVision" + "-q");
                  pred3Policies = pred3->getLocalPolicyNames();
                  std::vector<std::string>::iterator pred3PoliciesIterator = pred3Policies.begin();
                  while (pred3PoliciesIterator != pred3Policies.end())
                    {
                      //can do this as we are the source and target we are mapping are the same policy
                      pred1->addPTLMapping((*pred3PoliciesIterator), (*pred3PoliciesIterator), pred1QTable, pred3QTable);
                      pred2->addPTLMapping((*pred3PoliciesIterator), (*pred3PoliciesIterator), pred2QTable, pred3QTable);
                      pred3->addPTLMapping((*pred3PoliciesIterator), (*pred3PoliciesIterator), pred3QTable, pred1QTable);
                      pred3->addPTLMapping((*pred3PoliciesIterator), (*pred3PoliciesIterator), pred3QTable, pred2QTable);
                      pred3PoliciesIterator++;
                    }
                }
              if (NUMBER_OF_PREDITORS > 3)
                {
                  pred4QTable->readStateActionFromFile(OUT_PATH() + "+" + pred4->GetLocalName() + "+Local+" + "PredVision" + "-q");
                  pred4Policies = pred4->getLocalPolicyNames();
                  std::vector<std::string>::iterator pred4PoliciesIterator = pred4Policies.begin();
                  while (pred4PoliciesIterator != pred4Policies.end())
                    {
                      //can do this as we are the source and target we are mapping are the same policy
                      pred1->addPTLMapping((*pred4PoliciesIterator), (*pred4PoliciesIterator), pred1QTable, pred4QTable);
                      pred2->addPTLMapping((*pred4PoliciesIterator), (*pred4PoliciesIterator), pred2QTable, pred4QTable);
                      pred3->addPTLMapping((*pred4PoliciesIterator), (*pred4PoliciesIterator), pred3QTable, pred4QTable);
                      pred4->addPTLMapping((*pred4PoliciesIterator), (*pred4PoliciesIterator), pred4QTable, pred1QTable);
                      pred4->addPTLMapping((*pred4PoliciesIterator), (*pred4PoliciesIterator), pred4QTable, pred2QTable);
                      pred4->addPTLMapping((*pred4PoliciesIterator), (*pred4PoliciesIterator), pred4QTable, pred3QTable);
                      pred4PoliciesIterator++;
                    }
                }
            }
          if (USE_VECTOR_POLICY)
            {
              std::cout << "WARNNING DONT USE WITHOUT FIXING MAPPING\n";
              std::shared_ptr<QTable> pred1QTable = (std::make_shared<QTable>());
              std::shared_ptr<QTable> pred2QTable = (std::make_shared<QTable>());

              pred1QTable->readStateActionFromFile(OUT_PATH() + "+" + pred1->GetLocalName() + "+Local+" + "PredRange" + "-q");
              pred2QTable->readStateActionFromFile(OUT_PATH() + "+" + pred2->GetLocalName() + "+Local+" + "PredRange" + "-q");

              pred1->addPTLMapping(pred2->GetLocalName(), pred2->GetLocalName(), pred1QTable, pred2QTable);
              pred2->addPTLMapping(pred1->GetLocalName(), pred2->GetLocalName(), pred2QTable, pred1QTable);

            }
          if (USE_OWN_COORD_POLICY)
            {
              std::cout << "WARNNING DONT USE WITHOUT FIXING MAPPING\n";
              std::shared_ptr<QTable> pred1QTable = (std::make_shared<QTable>());
              std::shared_ptr<QTable> pred2QTable = (std::make_shared<QTable>());
              std::shared_ptr<QTable> pred3QTable = (std::make_shared<QTable>());
              pred1QTable->readStateActionFromFile(OUT_PATH() + "+" + pred1->GetLocalName() + "+Local+" + "PredCoord" + "-q");
              pred2QTable->readStateActionFromFile(OUT_PATH() + "+" + pred2->GetLocalName() + "+Local+" + "PredCoord" + "-q");
              //pred3QTable->readStateActionFromFile(CURRENT_PATH() + "+" + pred3->GetLocalName() + "+Local+" + "PredCoord" + "-q");
              pred1->addPTLMapping(pred2->GetLocalName(), pred2->GetLocalName(), pred1QTable, pred2QTable);
              pred2->addPTLMapping(pred1->GetLocalName(), pred2->GetLocalName(), pred2QTable, pred1QTable);
              /*pred1->addPTLMapping(pred2->GetLocalName(), pred1QTable, pred3QTable);
              pred2->addPTLMapping(pred1->GetLocalName(), pred2QTable, pred3QTable);
              pred3->addPTLMapping(pred3->GetLocalName(), pred1QTable, pred2QTable);
              pred3->addPTLMapping(pred3->GetLocalName(), pred2QTable, pred1QTable);*/
            }
          if (internalRun == start)
            {
              pred1->printPTLMappings(OUT_PATH() + "theCoordMap", EXP_NAME());
            }
        }
      std::shared_ptr<Prey> prey1 = (std::make_shared<Prey>());
      prey1->SetLocalName("A");
      prey1->SetPosition(std::pair<int, int>(3, 4));

      std::shared_ptr<Prey> prey2 = (std::make_shared<Prey>());
      prey2->SetLocalName("B");
      prey2->SetPosition(std::pair<int, int>(5, 4));

      std::shared_ptr<Prey> prey3 = (std::make_shared<Prey>());
      prey3->SetLocalName("C");
      prey3->SetPosition(std::pair<int, int>(2, 4));
      if (NUMBER_OF_PREDITORS > 0)
        {
          w->addPreditor(pred1);
        }
      if (NUMBER_OF_PREDITORS > 1)
        {
          w->addPreditor(pred2);
        }
      if (NUMBER_OF_PREDITORS > 2)
        {
          w->addPreditor(pred3);
        }
      if (NUMBER_OF_PREDITORS > 3)
        {
          w->addPreditor(pred4);
        }
      /*w->addPreditor(pred5);*/

      w->addPrey(prey1);
      //w->addPrey(prey2);
      // w->addPrey(prey3);
      //prey1->SetUsingRandomMove(true);
      prey1->SetUsingLines(true);
      //prey1->SetUsingLines(true);
      prey1->SetVisionRange(PREY_VISION_RANGE);
      //prey3->SetUsingEvasionMove(true);
      //prey3->SetUsingLines(true);
      //prey3->SetVisionRange(PREY_VISION_RANGE);
      //prey2->SetUsingEvasionMove(true);
      prey2->SetVisionRange(PREY_VISION_RANGE);
      prey2->SetUsingLines(true);

      ss.str("");
      ss << OUT_PATH() << runNumberString << "_output" << "_" << EXP_NAME() << ".ppstream";
      w->printToFile("", false, ss.str());
      ss.str("");
      double currentExploreTemp = 2000; //start at max temp
      double explorationDrop = (currentExploreTemp / EXPLORE_LENGTH);
      int testIndex = 0;
      for (int a = 0; a < EXPERIMENT_LENGTH; a++)
        {
          //std::cout << "1 Step: " << a << "\n";
          if ((a + 1) == TEST_TIMES[testIndex])//+1 for convince of it happening at end of run
            {
              std::stringstream ss;
              ss << EXP_NAME() << "+" << runNumberString << "+" << TEST_TIMES[testIndex];
              //dump agents info to read and test
              std::string mainName = ss.str();
              ss.str("");
              ss << mainName << "+P1";
              if (NUMBER_OF_PREDITORS > 0)
                {
                  pred1->publishRLStateSpace(OUT_PATH(), "plain");
                  ss.str("");
                  ss << mainName << "+P2";
                }
              if (NUMBER_OF_PREDITORS > 1)
                {
                  pred2->publishRLStateSpace(OUT_PATH(), "plain");
                  ss.str("");
                  ss << mainName << "+P3";
                }
              if (NUMBER_OF_PREDITORS > 2)
                {
                  pred3->publishRLStateSpace(OUT_PATH(), "plain");
                  ss.str("");
                  ss << mainName << "+P4";
                }
              if (NUMBER_OF_PREDITORS > 3)
                {
                  pred4->publishRLStateSpace(OUT_PATH(), "plain");
                  ss.str("");
                  ss << mainName << "+P5";
                }
              evaluateBranchPerformance(runNumberString, TEST_TIMES[testIndex]);
              testIndex++; //move on
              if (sizeof (TEST_TIMES) / sizeof (int)<testIndex)
                {//if at end of array loop back for safety
                  testIndex = 0;
                }
            }
          if (a == EXPLORE_LENGTH)
            {//finished learning
              w->setExploitaion();
            }
          //if should move 1 step down temperature
          currentExploreTemp = currentExploreTemp - explorationDrop;
          w->downTemperaturePreditor(currentExploreTemp);
          //std::cout << "temp= " << currentExploreTemp << " drop= " << explorationDrop << "\n";
          if (usingPTL)//&& a > EXPLORE_LENGTH)
            {
              if (NUMBER_OF_PREDITORS > 1)
                {
                  pred1->addPTLTransfers(pred2->getPTLTransfers());
                  pred2->addPTLTransfers(pred1->getPTLTransfers());
                }
              if (NUMBER_OF_PREDITORS > 2)
                {
                  pred1->addPTLTransfers(pred3->getPTLTransfers());
                  pred2->addPTLTransfers(pred3->getPTLTransfers());
                  pred3->addPTLTransfers(pred2->getPTLTransfers());
                  pred3->addPTLTransfers(pred1->getPTLTransfers());
                }
              if (NUMBER_OF_PREDITORS > 3)
                {
                  pred1->addPTLTransfers(pred4->getPTLTransfers());
                  pred2->addPTLTransfers(pred4->getPTLTransfers());
                  pred3->addPTLTransfers(pred4->getPTLTransfers());
                  pred4->addPTLTransfers(pred3->getPTLTransfers());
                  pred4->addPTLTransfers(pred2->getPTLTransfers());
                  pred4->addPTLTransfers(pred1->getPTLTransfers());
                }

            }
          //std::cout << "2 Step: " << a << "\n";
          w->printToFile(w->serialiseWorld());
          w->timeStep("RL", false);
          //std::cout << "3 Step: " << a << "\n";
        }
      w->printToFile("", true);
      if (NUMBER_OF_PREDITORS > 0)
        {
          ss << OUT_PATH() << runNumberString << "_EndOfRun";
          pred1->writeRLPolicies(ss.str(), EXP_NAME());
        }
      if (NUMBER_OF_PREDITORS > 1)
        {
          ss.str("");
          ss << OUT_PATH() << runNumberString << "_EndOfRun";
          pred2->writeRLPolicies(ss.str(), EXP_NAME());
        }
      if (NUMBER_OF_PREDITORS > 2)
        {
          ss.str("");
          ss << OUT_PATH() << runNumberString << "_-EndOfRun";
          pred3->writeRLPolicies(ss.str(), EXP_NAME());
        }
      if (NUMBER_OF_PREDITORS > 3)
        {
          ss.str("");
          ss << OUT_PATH() << runNumberString << "_-EndOfRun";
          pred4->writeRLPolicies(ss.str(), EXP_NAME());
        }
      ss.str("");
      ss << OUT_PATH() << runNumberString << "_EndOfRun";
      w->printTracesToFile(ss.str(), EXP_NAME());
    }

  /*std::stringstream ss;

    ss.str("");
    {
        ss << "cmd /c " << CURRENT_PATH() << "moveFiles.bat " << CURRENT_PATH() << " " << EXP_NAME() << " stats";
        system(ss.str().c_str());
    }*/
  return 1;
}

void evaluateBranchPerformance(std::string runNumberFormatted, int testTime)
{
  int internalRun = -1;
  int stop = -1;
  std::stringstream ss;
  std::cout << "-------------current exp is " << testTime << " of " << runNumberFormatted << "\n";
  std::shared_ptr<World> w = (std::make_shared<World>());
  std::shared_ptr<Preditor> pred1;
  std::shared_ptr<Preditor> pred2;
  std::shared_ptr<Preditor> pred3;
  std::shared_ptr<Preditor> pred4;

  //dump agents info to read and test
  if (NUMBER_OF_PREDITORS > 0)
    {
      ss.str("");
      ss << "atest";
      pred1 = (std::make_shared<Preditor>(ss.str(), WORLD_WIDTH, WORLD_HIGHT, PREDITOR_VISION_RANGE));
      ss.str("");
      ss << OUT_PATH() << "+a+Local+PredVision";
      pred1->SetPosition(std::pair<int, int>(3, 3));
      pred1->readPolicies(ss.str());
      pred1->publishRLStateSpace(OUT_PATH(), "plain"); //for ptl
      pred1->setExplotation();
      pred1->setTestTime(testTime);
    }
  if (NUMBER_OF_PREDITORS > 1)
    {
      ss.str("");
      ss << "btest";
      pred2 = (std::make_shared<Preditor>(ss.str(), WORLD_WIDTH, WORLD_HIGHT, PREDITOR_VISION_RANGE));
      ss.str("");
      ss << OUT_PATH() << "+b+Local+PredVision";
      pred2->SetPosition(std::pair<int, int>(4, 4));
      pred2->readPolicies(ss.str());
      pred2->publishRLStateSpace(OUT_PATH(), "plain"); //for ptl
      pred2->setExplotation();
      pred2->setTestTime(testTime);
    }
  if (NUMBER_OF_PREDITORS > 2)
    {

      ss.str("");
      ss << "ctest";
      pred3 = (std::make_shared<Preditor>(ss.str(), WORLD_WIDTH, WORLD_HIGHT, PREDITOR_VISION_RANGE));
      ;
      ss.str("");
      ss << OUT_PATH() << "+c+Local+PredVision";
      pred3->SetPosition(std::pair<int, int>(4, 3));
      pred3->readPolicies(ss.str());
      pred3->publishRLStateSpace(OUT_PATH(), "plain"); //for ptl
      pred3->setExplotation();
      pred3->setTestTime(testTime);
    }
  if (NUMBER_OF_PREDITORS > 3)
    {
      ss.str("");
      ss << "dtest";
      pred4 = (std::make_shared<Preditor>(ss.str(), WORLD_WIDTH, WORLD_HIGHT, PREDITOR_VISION_RANGE));
      ss.str("");
      ss << OUT_PATH() << "+d+Local+PredVision";
      pred4->SetPosition(std::pair<int, int>(4, 2));
      pred4->readPolicies(ss.str());
      pred4->publishRLStateSpace(OUT_PATH(), "plain"); //for ptl
      pred4->setExplotation();
      pred4->setTestTime(testTime);
    }
  //now make a mapping
  if (usingPTL)
    {
      if (USE_VISION_POLICY)
        {
          std::shared_ptr<QTable> pred1QTable = (std::make_shared<QTable>());
          std::vector<std::string> pred1Policies;
          std::shared_ptr<QTable> pred2QTable = (std::make_shared<QTable>());
          std::vector<std::string> pred2Policies;
          std::shared_ptr<QTable> pred3QTable = (std::make_shared<QTable>());
          std::vector<std::string> pred3Policies;
          std::shared_ptr<QTable> pred4QTable = (std::make_shared<QTable>());
          std::vector<std::string> pred4Policies;
          if (NUMBER_OF_PREDITORS > 0)
            {
              pred1QTable->readStateActionFromFile(OUT_PATH() + "+" + pred1->GetLocalName() + "+Local+" + "PredVision" + "-q");
              pred1Policies = pred1->getLocalPolicyNames();
            }
          if (NUMBER_OF_PREDITORS > 1)
            {
              pred2QTable->readStateActionFromFile(OUT_PATH() + "+" + pred2->GetLocalName() + "+Local+" + "PredVision" + "-q");
              pred2Policies = pred2->getLocalPolicyNames();
              std::vector<std::string>::iterator pred1PoliciesIterator = pred1Policies.begin();
              while (pred1PoliciesIterator != pred1Policies.end())
                {
                  //can do this as we are the source and target we are mapping are the same policy
                  pred1->addPTLMapping((*pred1PoliciesIterator), (*pred1PoliciesIterator), pred1QTable, pred2QTable);
                  pred1PoliciesIterator++;
                }
              std::vector<std::string>::iterator pred2PoliciesIterator = pred2Policies.begin();
              while (pred2PoliciesIterator != pred2Policies.end())
                {
                  //can do this as we are the source and target we are mapping are the same policy
                  pred2->addPTLMapping((*pred2PoliciesIterator), (*pred2PoliciesIterator), pred2QTable, pred1QTable);
                  pred2PoliciesIterator++;
                }
              //pred2->addPTLMapping(pred1->GetLocalName(), pred2QTable, pred1QTable);
              //pred1->addPTLMapping(pred2->GetLocalName(), pred1QTable, pred2QTable);
            }
          if (NUMBER_OF_PREDITORS > 2)
            {
              pred3QTable->readStateActionFromFile(OUT_PATH() + "+" + pred3->GetLocalName() + "+Local+" + "PredVision" + "-q");
              pred3Policies = pred3->getLocalPolicyNames();
              std::vector<std::string>::iterator pred3PoliciesIterator = pred3Policies.begin();
              while (pred3PoliciesIterator != pred3Policies.end())
                {
                  //can do this as we are the source and target we are mapping are the same policy
                  pred1->addPTLMapping((*pred3PoliciesIterator), (*pred3PoliciesIterator), pred1QTable, pred3QTable);
                  pred2->addPTLMapping((*pred3PoliciesIterator), (*pred3PoliciesIterator), pred2QTable, pred3QTable);
                  pred3->addPTLMapping((*pred3PoliciesIterator), (*pred3PoliciesIterator), pred3QTable, pred1QTable);
                  pred3->addPTLMapping((*pred3PoliciesIterator), (*pred3PoliciesIterator), pred3QTable, pred2QTable);
                  pred3PoliciesIterator++;
                }
            }
          if (NUMBER_OF_PREDITORS > 3)
            {
              pred4QTable->readStateActionFromFile(OUT_PATH() + "+" + pred4->GetLocalName() + "+Local+" + "PredVision" + "-q");
              pred4Policies = pred4->getLocalPolicyNames();
              std::vector<std::string>::iterator pred4PoliciesIterator = pred4Policies.begin();
              while (pred4PoliciesIterator != pred4Policies.end())
                {
                  //can do this as we are the source and target we are mapping are the same policy
                  pred1->addPTLMapping((*pred4PoliciesIterator), (*pred4PoliciesIterator), pred1QTable, pred4QTable);
                  pred2->addPTLMapping((*pred4PoliciesIterator), (*pred4PoliciesIterator), pred2QTable, pred4QTable);
                  pred3->addPTLMapping((*pred4PoliciesIterator), (*pred4PoliciesIterator), pred3QTable, pred4QTable);
                  pred4->addPTLMapping((*pred4PoliciesIterator), (*pred4PoliciesIterator), pred4QTable, pred1QTable);
                  pred4->addPTLMapping((*pred4PoliciesIterator), (*pred4PoliciesIterator), pred4QTable, pred2QTable);
                  pred4->addPTLMapping((*pred4PoliciesIterator), (*pred4PoliciesIterator), pred4QTable, pred3QTable);
                  pred4PoliciesIterator++;
                }
            }
        }
    }
  if (NUMBER_OF_PREDITORS > 0)
    {
      w->addPreditor(pred1);
    }
  if (NUMBER_OF_PREDITORS > 1)
    {
      w->addPreditor(pred2);
    }
  if (NUMBER_OF_PREDITORS > 2)
    {
      w->addPreditor(pred3);
    }
  if (NUMBER_OF_PREDITORS > 3)
    {
      w->addPreditor(pred4);
    }
  //add prey
  std::shared_ptr<Prey> prey1 = (std::make_shared<Prey>());
  prey1->SetLocalName("A");
  prey1->SetPosition(std::pair<int, int>(3, 4));

  std::shared_ptr<Prey> prey2 = (std::make_shared<Prey>());
  prey2->SetLocalName("B");
  prey2->SetPosition(std::pair<int, int>(5, 4));

  std::shared_ptr<Prey> prey3 = (std::make_shared<Prey>());
  prey3->SetLocalName("C");
  prey3->SetPosition(std::pair<int, int>(2, 4));
  w->addPrey(prey1);
  // w->addPrey(prey2);
  // w->addPrey(prey3);
  //prey1->SetUsingRandomMove(true);
  prey1->SetUsingEvasionMove(true);
  //prey1->SetUsingLines(true);
  prey1->SetVisionRange(PREY_VISION_RANGE);
  prey3->SetUsingEvasionMove(true);
  //prey3->SetUsingLines(true);
  prey3->SetVisionRange(PREY_VISION_RANGE);
  prey2->SetUsingEvasionMove(true);
  //prey2->SetVisionRange(PREY_VISION_RANGE);
  //prey2->SetUsingLines(true);
  //new agents madw now run them
  ss.str("");
  ss << OUT_PATH() << runNumberFormatted << "_test_" << testTime << "_output" << "_" << EXP_NAME() << ".ppstream";
  w->printToFile("", false, ss.str());
  ss.str("");
  for (int a = 0; a < EXPERIMENT_TEST_LENGTH; a++)
    {
      //std::cout << "1 Step: " << a << "\n";

      //std::cout << "temp= " << currentExploreTemp << " drop= " << explorationDrop << "\n";
      if (usingPTL)//&& a > EXPLORE_LENGTH)
        {
          if (NUMBER_OF_PREDITORS > 1)
            {
              pred1->addPTLTransfers(pred2->getPTLTransfers());
              pred2->addPTLTransfers(pred1->getPTLTransfers());
            }
          if (NUMBER_OF_PREDITORS > 2)
            {
              pred1->addPTLTransfers(pred3->getPTLTransfers());
              pred2->addPTLTransfers(pred3->getPTLTransfers());
              pred3->addPTLTransfers(pred2->getPTLTransfers());
              pred3->addPTLTransfers(pred1->getPTLTransfers());
            }
          if (NUMBER_OF_PREDITORS > 3)
            {
              pred1->addPTLTransfers(pred4->getPTLTransfers());
              pred2->addPTLTransfers(pred4->getPTLTransfers());
              pred3->addPTLTransfers(pred4->getPTLTransfers());
              pred4->addPTLTransfers(pred3->getPTLTransfers());
              pred4->addPTLTransfers(pred2->getPTLTransfers());
              pred4->addPTLTransfers(pred1->getPTLTransfers());
            }

        }
      //std::cout << "2 Step: " << a << "\n";
      w->printToFile(w->serialiseWorld());
      w->timeStep("RL", false);
      //std::cout << "3 Step: " << a << "\n";
    }
  w->printToFile("", true);
  if (NUMBER_OF_PREDITORS > 0)
    {
      ss << OUT_PATH() << runNumberFormatted << "_test_" << testTime << "_EndOfRun";
      pred1->writeRLPolicies(ss.str(), EXP_NAME());
    }
  if (NUMBER_OF_PREDITORS > 1)
    {
      ss.str("");
      ss << OUT_PATH() << runNumberFormatted << "_test_" << testTime << "_EndOfRun";
      pred2->writeRLPolicies(ss.str(), EXP_NAME());
    }
  if (NUMBER_OF_PREDITORS > 2)
    {
      ss.str("");
      ss << OUT_PATH() << runNumberFormatted << "_test_" << testTime << "_EndOfRun";
      pred3->writeRLPolicies(ss.str(), EXP_NAME());
    }
  if (NUMBER_OF_PREDITORS > 3)
    {
      ss.str("");
      ss << OUT_PATH() << runNumberFormatted << "_test_" << testTime << "_EndOfRun";
      pred4->writeRLPolicies(ss.str(), EXP_NAME());
    }
  ss.str("");
  ss << OUT_PATH() << runNumberFormatted << "_test_" << testTime << "_EndOfRun";
  w->printTracesToFile(ss.str(), EXP_NAME());

}
