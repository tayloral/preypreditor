/* 
 * File:   PreditorReward.cpp
 * Author: Adam
 * 
 * Created on 31 May 2016, 15:27
 */

#include "PreditorVectorReward.h"
#include "Constants.h"
#include <string>
#include <iostream>
#include <cstdlib>

PreditorVectorReward::PreditorVectorReward()
{
}

PreditorVectorReward::~PreditorVectorReward()
{
}

void PreditorVectorReward::calcReward()
{
    std::cout << "calc reward 0 arg\n";
    reward = 1;
}

/**
 *not used anymore but need for an interface 
 */
void PreditorVectorReward::calcReward(std::string in)
{
    std::cout << "calc reward 1 arg\n";
    reward = 2; //as exciting as the non-passed version
}

/**
 *this is the version called in the normal RL update cycle.
 * It sets the reward variable to be used.  In this case the maze calculates it for us, so it does nothing. 
 */
void PreditorVectorReward::calcReward(std::string oldState, std::string worldState)
{
    //std::cout << "oldState= " << oldState << " worldstate= " << worldState << "\n";
    std::string predDist = worldState.substr(0, worldState.find("_"));
    worldState = worldState.substr(worldState.find("_") + 1, worldState.length() - (worldState.find("_") + 1));
    //std::cout << "oldState= " << oldState << " worldstate= " << worldState << "\n";
    std::string preyDist = worldState.substr(0, worldState.find("_"));
    worldState = worldState.substr(worldState.find("_") + 1, worldState.length() - (worldState.find("_") + 1));
    std::string angle = worldState;


    if (std::atoi(predDist.c_str()) == 0 || std::atoi(preyDist.c_str()) == 0)
    {
        reward = 0;
    }
    else
    {
        //std::cout << "predDist= " << predDist << "\n";
        //std::cout << "preyDist= " << preyDist << "\n";
        //std::cout << "angle= " << angle << "\n";
        double angleReward = 100 * ((180 / BIN_SIZE_DEGREES) - std::atoi(angle.c_str())); //are we on the right side
        double distanceReward = 0;
        distanceReward += 100 * ((PREDITOR_VISION_RANGE + 1) - std::atoi(preyDist.c_str()));
        distanceReward += 100 * ((PREDITOR_VISION_RANGE + 1) - std::atoi(predDist.c_str()));

        reward = distanceReward + angleReward;

    }

    if (moveResult == MOVE_RESULTS::PRED_INTO_PREY || moveResult == MOVE_RESULTS::PREY_INTO_PRED || moveResult == MOVE_RESULTS::ANOTHER_CAUGHT)
    {//not caught
        reward += 10000; //simulate eating prey
        //std::cout << "PreditorReward::calcReward caught prey\n";
    }
    else if (moveResult == MOVE_RESULTS::INTO_IMPASSABLE || moveResult == MOVE_RESULTS::SUCCESS)
    {
        //reward = 0; //simulate hunger and running into a wall pain
    }
    else
    {
        std::cout << "PreditorReward::calcReward unknown move outcome\n";
        std::exit(54242);
    }
    // std::cout << "reward= " << reward << "\n";
}

void PreditorVectorReward::setMoveResult(MOVE_RESULTS moveResultIn)
{
    moveResult = moveResultIn;
}