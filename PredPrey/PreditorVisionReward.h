#include "Reward.h"
#include "Constants.h"
/* 
 * File:   PreditorVisionReward.h
 * Author: Adam
 *
 * Created on 08 June 2016, 12:07
 */

#ifndef PREDITORVISIONREWARD_H
#define PREDITORVISIONREWARD_H

class PreditorVisionReward : public Reward {
public:
    PreditorVisionReward();
    void calcReward();
    void calcReward(std::string in);
    void calcReward(std::string oldState, std::string worldState);
    void setReward(double rewardIn);
    void setMoveResult(MOVE_RESULTS moveResultIn);
    virtual ~PreditorVisionReward();
private:
    MOVE_RESULTS moveResult;
};

#endif /* PREDITORVISIONREWARD_H */

