/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   Constants.h
 * Author: Adam
 *
 * Created on 08 July 2016, 10:11
 */

#ifndef CONSTANTS_H
#define CONSTANTS_H

#define START_NUMBER_OF_EXPERIMENT 0
#define NUMBER_OF_RUNS 200
#define EXPERIMENT_TEST_LENGTH 3000
#define EXPERIMENT_LENGTH EXPERIMENT_TEST_LENGTH
#define EXPLORE_LENGTH EXPERIMENT_TEST_LENGTH
#define NUMBER_OF_PREDITORS 4

#define WORLD_WIDTH 10
#define WORLD_HIGHT 10
#define NUMBER_OF_MOORE_ACTIONS 5
#define PREDITOR_VISION_RANGE 2
#define USING_CROSS_VISION false
#define PREY_VISION_RANGE 1
#define PREDITOR_STEPS_PER_TIMESTEP 1
#define PREY_STEPS_TO_PREDITOR .3
#define EVASION_CONSTANT_TIME 1
#define USING_EVASION_COOLDOWN true
#define WORLD_SPHERICAL false
#define ALL_AGENTS_SHARE_REWARD false
#define USE_OWN_COORD_POLICY !true
#define USE_VECTOR_POLICY !true
#define USE_VISION_POLICY true
#define MULTI_FEATURE_VISION false
#define BIN_SIZE_DEGREES 45
#define DEGREES_PER_RADIAN 57.2958
#define usingPTL !true//false//

static const std::string EXP_NAME() {
    //return "RLTest"; //
    //return "PTLTest"; // a0.0_b0.0_no0_pv0_fv0_fc0_co1_vi1_ve1
    return "RL+norm+third3speedShort+pr4v1+py2Line+10"; //
}

static const std::string OUT_PATH() {
    return "C:/Users/Work-mine/Documents/WorkingDir/PredPrey/" + EXP_NAME() + "/";
}

static const std::string BAT_PATH() {
    return "C:/Users/Work-mine/Documents/Code/preypreditor/PredPrey/";
}

enum MOVE_RESULTS {
    SUCCESS,
    PRED_INTO_PREY,
    PREY_INTO_PRED,
    INTO_IMPASSABLE,
    ANOTHER_CAUGHT
};

static const std::string TIMESTEP_TYPE_RL() {
    return "RL";
};
static const int TEST_TIMES[] = {
    1,
    10,
    20,
    30,
    40,
    50,
    60,
    70,
    80,
    90,
    100,
    200,
    300,
    400,
    500,
    1000,
    1500,
    2000,
    2500,
    3000,
    4000,
    5000,
    7500,
    10000

};

static const std::string ACTIONS_STRINGS[9] = {
    "North",
    "East",
    "South",
    "West",
    "Pause",
    "North East",
    "South East",
    "North West",
    "South West"
};

enum ACTIONS {
    N,
    E,
    S,
    W,
    P,
    NE,
    SE,
    SW,
    NW,
    END_OF_ENUM
};

enum SQUARE_TYPE {
    EMPTY,
    PREDITOR_ONLY,
    PREY_ONLY,
    IMPASSABLE,
    WALL,
    last
};
#endif /* CONSTANTS_H */

