/* 
 * File:   Prey.h
 * Author: Adam
 *
 * Created on 08 April 2016, 15:41
 */
#include <string>
#include <utility>
#include <vector>
#include "Constants.h"
#ifndef PREY_H
#define PREY_H

class Prey {
public:
    Prey();
    Prey(const Prey& orig);
    virtual ~Prey();
    void SetStamina(int stamina);
    int GetStamina() const;
    void SetSprintSpeed(int sprintSpeed);
    int GetSprintSpeed() const;
    void SetSpeed(int speed);
    int GetSpeed() const;
    void SetVisionRange(int visionRange);
    int GetVisionRange() const;
    void SetPosition(std::pair<int, int> position);
    std::pair<int, int> GetPosition() const;
    void SetLocalName(std::string name);
    std::string GetLocalName() const;
    void SetRlName(std::string rlName);
    std::string GetRlName() const;
    std::string serialisePrey();
    ACTIONS randomMove();
    ACTIONS moveInLines();
    ACTIONS getAction();
    void notifyOfResults(MOVE_RESULTS result, std::pair<int, int> location);
    void SetUsingEvasionMove(bool usingEvasionMoveIn);
    bool IsUsingEvasionMove() const;
    void SetUsingLines(bool usingLines);
    bool IsUsingLines() const;
    void SetUsingRandomMove(bool usingRandomMove);
    bool IsUsingRandomMove() const;
    void SetUsingStopMove(bool usingStopMove);
    bool IsUsingStopMove() const;
    std::string printTrace();
    void printReward(std::string fileName, std::string tag);
    void setExplotation();
    ACTIONS getEvasionMoveAction(bool hitWall);
    void SetPreditorsSeen(std::vector<bool> preditorsSeen);
private:
    int visionRange;
    int speed; //not used
    int sprintSpeed;
    int stamina;
    ACTIONS currentLineDirection;
    std::pair<int, int> position;
    std::string localName;
    std::string rlName;
    bool moore;
    bool vanN;
    bool usingRandomMove;
    bool usingEvasionMove;
    bool usingStopMove;
    bool usingLines;
    bool usingRL;
    std::vector<std::pair<int, int>> trace;
    std::vector<bool> preditorsSeen;
    int evasionCoolDown;
    int stepsUntillMovement;
};

#endif /* PREY_H */

