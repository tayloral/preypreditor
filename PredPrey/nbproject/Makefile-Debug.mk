#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=MinGW_64-Windows
CND_DLIB_EXT=dll
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/NoSharedMemTL/src/ActionSelection.o \
	${OBJECTDIR}/NoSharedMemTL/src/Boltzmann.o \
	${OBJECTDIR}/NoSharedMemTL/src/CollaborationProcess.o \
	${OBJECTDIR}/NoSharedMemTL/src/Cusum.o \
	${OBJECTDIR}/NoSharedMemTL/src/DWLAgent.o \
	${OBJECTDIR}/NoSharedMemTL/src/EGready.o \
	${OBJECTDIR}/NoSharedMemTL/src/NeighbourReward.o \
	${OBJECTDIR}/NoSharedMemTL/src/Policy.o \
	${OBJECTDIR}/NoSharedMemTL/src/QTable.o \
	${OBJECTDIR}/NoSharedMemTL/src/Reward.o \
	${OBJECTDIR}/NoSharedMemTL/src/TaylorSeriesSelection.o \
	${OBJECTDIR}/NoSharedMemTL/src/TransferMapping.o \
	${OBJECTDIR}/NoSharedMemTL/src/WLearningProcess.o \
	${OBJECTDIR}/NoSharedMemTL/src/WTable.o \
	${OBJECTDIR}/Preditor.o \
	${OBJECTDIR}/PreditorAgent.o \
	${OBJECTDIR}/PreditorCoordReward.o \
	${OBJECTDIR}/PreditorVectorReward.o \
	${OBJECTDIR}/PreditorVisionReward.o \
	${OBJECTDIR}/Prey.o \
	${OBJECTDIR}/World.o \
	${OBJECTDIR}/main.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=-static-libgcc -static-libstdc++ -static -lpthread
CXXFLAGS=-static-libgcc -static-libstdc++ -static -lpthread

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/1predprey.exe

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/1predprey.exe: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/1predprey ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/NoSharedMemTL/src/ActionSelection.o: NoSharedMemTL/src/ActionSelection.cpp nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} -p ${OBJECTDIR}/NoSharedMemTL/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -INoSharedMemTL/include -I. -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/NoSharedMemTL/src/ActionSelection.o NoSharedMemTL/src/ActionSelection.cpp

${OBJECTDIR}/NoSharedMemTL/src/Boltzmann.o: NoSharedMemTL/src/Boltzmann.cpp nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} -p ${OBJECTDIR}/NoSharedMemTL/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -INoSharedMemTL/include -I. -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/NoSharedMemTL/src/Boltzmann.o NoSharedMemTL/src/Boltzmann.cpp

${OBJECTDIR}/NoSharedMemTL/src/CollaborationProcess.o: NoSharedMemTL/src/CollaborationProcess.cpp nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} -p ${OBJECTDIR}/NoSharedMemTL/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -INoSharedMemTL/include -I. -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/NoSharedMemTL/src/CollaborationProcess.o NoSharedMemTL/src/CollaborationProcess.cpp

${OBJECTDIR}/NoSharedMemTL/src/Cusum.o: NoSharedMemTL/src/Cusum.cpp nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} -p ${OBJECTDIR}/NoSharedMemTL/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -INoSharedMemTL/include -I. -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/NoSharedMemTL/src/Cusum.o NoSharedMemTL/src/Cusum.cpp

${OBJECTDIR}/NoSharedMemTL/src/DWLAgent.o: NoSharedMemTL/src/DWLAgent.cpp nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} -p ${OBJECTDIR}/NoSharedMemTL/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -INoSharedMemTL/include -I. -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/NoSharedMemTL/src/DWLAgent.o NoSharedMemTL/src/DWLAgent.cpp

${OBJECTDIR}/NoSharedMemTL/src/EGready.o: NoSharedMemTL/src/EGready.cpp nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} -p ${OBJECTDIR}/NoSharedMemTL/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -INoSharedMemTL/include -I. -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/NoSharedMemTL/src/EGready.o NoSharedMemTL/src/EGready.cpp

${OBJECTDIR}/NoSharedMemTL/src/NeighbourReward.o: NoSharedMemTL/src/NeighbourReward.cpp nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} -p ${OBJECTDIR}/NoSharedMemTL/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -INoSharedMemTL/include -I. -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/NoSharedMemTL/src/NeighbourReward.o NoSharedMemTL/src/NeighbourReward.cpp

${OBJECTDIR}/NoSharedMemTL/src/Policy.o: NoSharedMemTL/src/Policy.cpp nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} -p ${OBJECTDIR}/NoSharedMemTL/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -INoSharedMemTL/include -I. -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/NoSharedMemTL/src/Policy.o NoSharedMemTL/src/Policy.cpp

${OBJECTDIR}/NoSharedMemTL/src/QTable.o: NoSharedMemTL/src/QTable.cpp nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} -p ${OBJECTDIR}/NoSharedMemTL/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -INoSharedMemTL/include -I. -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/NoSharedMemTL/src/QTable.o NoSharedMemTL/src/QTable.cpp

${OBJECTDIR}/NoSharedMemTL/src/Reward.o: NoSharedMemTL/src/Reward.cpp nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} -p ${OBJECTDIR}/NoSharedMemTL/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -INoSharedMemTL/include -I. -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/NoSharedMemTL/src/Reward.o NoSharedMemTL/src/Reward.cpp

${OBJECTDIR}/NoSharedMemTL/src/TaylorSeriesSelection.o: NoSharedMemTL/src/TaylorSeriesSelection.cpp nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} -p ${OBJECTDIR}/NoSharedMemTL/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -INoSharedMemTL/include -I. -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/NoSharedMemTL/src/TaylorSeriesSelection.o NoSharedMemTL/src/TaylorSeriesSelection.cpp

${OBJECTDIR}/NoSharedMemTL/src/TransferMapping.o: NoSharedMemTL/src/TransferMapping.cpp nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} -p ${OBJECTDIR}/NoSharedMemTL/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -INoSharedMemTL/include -I. -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/NoSharedMemTL/src/TransferMapping.o NoSharedMemTL/src/TransferMapping.cpp

${OBJECTDIR}/NoSharedMemTL/src/WLearningProcess.o: NoSharedMemTL/src/WLearningProcess.cpp nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} -p ${OBJECTDIR}/NoSharedMemTL/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -INoSharedMemTL/include -I. -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/NoSharedMemTL/src/WLearningProcess.o NoSharedMemTL/src/WLearningProcess.cpp

${OBJECTDIR}/NoSharedMemTL/src/WTable.o: NoSharedMemTL/src/WTable.cpp nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} -p ${OBJECTDIR}/NoSharedMemTL/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -INoSharedMemTL/include -I. -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/NoSharedMemTL/src/WTable.o NoSharedMemTL/src/WTable.cpp

${OBJECTDIR}/Preditor.o: Preditor.cpp nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -INoSharedMemTL/include -I. -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Preditor.o Preditor.cpp

${OBJECTDIR}/PreditorAgent.o: PreditorAgent.cpp nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -INoSharedMemTL/include -I. -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/PreditorAgent.o PreditorAgent.cpp

${OBJECTDIR}/PreditorCoordReward.o: PreditorCoordReward.cpp nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -INoSharedMemTL/include -I. -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/PreditorCoordReward.o PreditorCoordReward.cpp

${OBJECTDIR}/PreditorVectorReward.o: PreditorVectorReward.cpp nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -INoSharedMemTL/include -I. -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/PreditorVectorReward.o PreditorVectorReward.cpp

${OBJECTDIR}/PreditorVisionReward.o: PreditorVisionReward.cpp nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -INoSharedMemTL/include -I. -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/PreditorVisionReward.o PreditorVisionReward.cpp

${OBJECTDIR}/Prey.o: Prey.cpp nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -INoSharedMemTL/include -I. -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Prey.o Prey.cpp

${OBJECTDIR}/World.o: World.cpp nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -INoSharedMemTL/include -I. -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/World.o World.cpp

${OBJECTDIR}/main.o: main.cpp nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -INoSharedMemTL/include -I. -std=c++14 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/main.o main.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
