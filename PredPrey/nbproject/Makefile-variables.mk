#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# Debug configuration
CND_PLATFORM_Debug=MinGW_64-Windows
CND_ARTIFACT_DIR_Debug=dist/Debug/MinGW_64-Windows
CND_ARTIFACT_NAME_Debug=1predprey
CND_ARTIFACT_PATH_Debug=dist/Debug/MinGW_64-Windows/1predprey
CND_PACKAGE_DIR_Debug=dist/Debug/MinGW_64-Windows/package
CND_PACKAGE_NAME_Debug=predprey.tar
CND_PACKAGE_PATH_Debug=dist/Debug/MinGW_64-Windows/package/predprey.tar
# Release configuration
CND_PLATFORM_Release=MinGW_64-Windows
CND_ARTIFACT_DIR_Release=dist/Release/MinGW_64-Windows
CND_ARTIFACT_NAME_Release=predprey
CND_ARTIFACT_PATH_Release=dist/Release/MinGW_64-Windows/predprey
CND_PACKAGE_DIR_Release=dist/Release/MinGW_64-Windows/package
CND_PACKAGE_NAME_Release=predprey.tar
CND_PACKAGE_PATH_Release=dist/Release/MinGW_64-Windows/package/predprey.tar
# Copy_of_Debug configuration
CND_PLATFORM_Copy_of_Debug=MinGW_64-Windows
CND_ARTIFACT_DIR_Copy_of_Debug=dist/Copy_of_Debug/MinGW_64-Windows
CND_ARTIFACT_NAME_Copy_of_Debug=predprey
CND_ARTIFACT_PATH_Copy_of_Debug=dist/Copy_of_Debug/MinGW_64-Windows/predprey
CND_PACKAGE_DIR_Copy_of_Debug=dist/Copy_of_Debug/MinGW_64-Windows/package
CND_PACKAGE_NAME_Copy_of_Debug=predprey.tar
CND_PACKAGE_PATH_Copy_of_Debug=dist/Copy_of_Debug/MinGW_64-Windows/package/predprey.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
