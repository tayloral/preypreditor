/*
 * File:   Preditor.cpp
 * Author: Adam
 *
 * Created on 08 April 2016, 15:41
 */

#include "Preditor.h"
#include "PreditorAgent.h"
#include <sstream>

Preditor::Preditor(std::string localNameIn, int width, int heigth, int visionRangeIn)
{
  testTime = 0;
  moore = true;
  vanN = false;
  visionRange = visionRangeIn;
  agent = std::make_shared<PreditorAgent>(localNameIn, width, heigth, visionRangeIn, 1);
  this->SetLocalName(localNameIn);
}

void Preditor::SetRlName(std::string rlName)
{
  this->rlName = rlName;
}

std::string Preditor::GetRlName() const
{
  return rlName;
}

void Preditor::setTestTime(int input)
{
  testTime = input;
}

Preditor::~Preditor() { }

void Preditor::SetStamina(int stamina)
{
  this->stamina = stamina;
}

int Preditor::GetStamina() const
{
  return stamina;
}

void Preditor::SetSprintSpeed(int sprintSpeed)
{
  this->sprintSpeed = sprintSpeed;
}

int Preditor::GetSprintSpeed() const
{
  return sprintSpeed;
}

void Preditor::SetSpeed(int speed)
{
  this->speed = speed;
}

int Preditor::GetSpeed() const
{
  return speed;
}

void Preditor::SetMoveResult(int moveResult)
{
  this->moveResult = moveResult;
}

int Preditor::GetMoveResult() const
{
  return moveResult;
}

void Preditor::SetVisionRange(int visionRange)
{
  this->visionRange = visionRange;
}

int Preditor::GetVisionRange() const
{
  return visionRange;
}

void Preditor::SetPosition(std::pair<int, int> position)
{
  this->position = position;
  trace.push_back(position);
}

void Preditor::printReward(std::string fileName, std::string tag)
{
  agent->printReward(fileName, tag);
}

std::pair<int, int> Preditor::GetPosition() const
{
  return position;
}

void Preditor::SetLocalName(std::string name)
{
  this->localName = name;
}

std::string Preditor::GetLocalName() const
{
  return localName;
}

std::string Preditor::serialisePreditor()
{
  std::stringstream ss;
  ss << "<Pred><posX><" << position.first << "><posY><" << position.second << "><spee><" << speed << "><sSpe><" << sprintSpeed << "><stam><" << stamina << "><visi><" << visionRange << "><locN><" << localName << "><rlNa><" << rlName << ">\n";
  return ss.str();
}

std::string Preditor::getRLAction()
{
  std::string action = agent->nominate();
  //std::cout << action << "\n";
  return action;
}

void Preditor::updateRLLocal(std::string localState, MOVE_RESULTS moveResult)
{
  //std::cout << " Preditor::updateRLLocal\n";
  agent->setMoveResult(moveResult);
  //std::cout << " 2\n";
  agent->updateLocal(localState);
  //std::cout << " 3\n";
  if ((moveResult == MOVE_RESULTS::PRED_INTO_PREY) || (moveResult == MOVE_RESULTS::PREY_INTO_PRED))
    {
      catches.push_back(1);
    }
  else
    {
      catches.push_back(0);
    }
}

std::string Preditor::PairToCoord(std::pair<int, int> input)
{
  std::stringstream ss;
  ss << input.first << "-" << input.second;
  return ss.str();
}

void Preditor::writeRLPolicies(std::string name, std::string tag)
{
  std::stringstream ss;
  ss << name << "+" << this->GetLocalName();
  agent->writePolicies(ss.str(), tag);
}

void Preditor::changeTemperature(int newOne)
{
  agent->changeActionSelectionTemperature(newOne);
}

std::string Preditor::printTrace()
{
  std::stringstream ss;
  std::vector < std::pair<int, int>>::iterator traceIt = trace.begin();
  while (traceIt != trace.end())
    {
      ss << traceIt->first << "," << traceIt->second << ",";
      traceIt++;
    }
  return ss.str();
}

std::string Preditor::printCatches()
{
  std::stringstream ss;
  std::vector <int>::iterator catchesIt = catches.begin();
  while (catchesIt != catches.end())
    {
      ss << (*catchesIt) << ",";
      catchesIt++;
    }
  return ss.str();
}

void Preditor::setExplotation()
{
  agent->changeActionSelectionTemperature(1);
  agent->manageLearning(false, false);

}

void Preditor::readPolicies(std::string name)
{
  agent->readPolicies(name);
}

void Preditor::publishRLStateSpace(std::string path, std::string tag)
{
  std::stringstream ss;
  ss << path << "+" << this->GetLocalName();
  agent->writePolicies(ss.str(), tag);
  //std::cout << ss.str() << " from pub local\n";
}

void Preditor::addPTLMapping(std::string sourceName, std::string targetName, std::shared_ptr<QTable> source, std::shared_ptr<QTable> target)
{
  agent->setUsingTransferLearning(true);
  agent->addSearchMapping(sourceName, targetName, source, target);
}

void Preditor::printPTLMappings(std::string filename, std::string tag)
{
  agent->printMappings(filename, 1, tag);
}

std::string Preditor::getPTLTransfers()
{
  return agent->transferToAllFromAll();
}

void Preditor::addPTLTransfers(std::string ptlIn)
{

  agent->readTransferedInfoIn(ptlIn);
}

std::vector<std::string> Preditor::getLocalPolicyNames()
{
  return agent->getLocalPolicyNames();
}