/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
#include <string>
#include "Reward.h"
#include "Constants.h"
/* 
 * File:   PreditorVectorReward.h
 * Author: Adam
 *
 * Created on 05 August 2016, 12:00
 */

#ifndef PREDITORVECTORREWARD_H
#define PREDITORVECTORREWARD_H

class PreditorVectorReward : public Reward {
public:
    PreditorVectorReward();
    void calcReward();
    void calcReward(std::string in);
    void calcReward(std::string oldState, std::string worldState);
    void setReward(double rewardIn);
    void setMoveResult(MOVE_RESULTS moveResultIn);
    virtual ~PreditorVectorReward();
private:
    MOVE_RESULTS moveResult;

};

#endif /* PREDITORVECTORREWARD_H */

