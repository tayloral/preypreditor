/* 
 * File:   Policy.h
 * Author: Adam
 *
 * Created on July 11, 2012, 9:24 AM
 */
#ifndef POLICY_H
#define POLICY_H

#include <string>

/**
 This encapsulates a way of selecting actions.  Not necessarily containing a DWL process.  This should allow for agent type heterogeneity.

  charris - I think it should just be a generic container and we downcast to do specific stuff to an implementation?
 */

/**
 as multiple types of change can happen this stors which is current*/
struct EnvrionmentChangeType {
    bool slowChange; //environment is changing at a rate less than or equal to learning speed
    bool fastChange; //environment is changing faster than learning rate
    bool sharpChnageHappened; //if many states changed
    bool processedSharpChnageHappened; //if many states are still changing but we have taken action
    bool iChanged; //if the agent has been reconfigured new policies etc.
    bool neighbourChange; //if the quality of transfer appears to change 
    bool iLearned; //if i hav finished learning
    bool processedILearned; //if i finished learning and we did what was to be done
    //time is maintained in the visitcount in q table

};

enum SelectionType {
    targetedBatchBased,
    targetedModelBased,
    greatestChange,
    mostVisit,
    randomSet,
    convergedMostVisit,
    manyVisit,
    best,
    DEFAULT_SELECTION
};

enum MergeType {
    shadowCopy,
    testMerge,
    adaptive,
    adaptiveProbabilistic,
    onlySource,
    onlyTarget,
    DEFAULT_MERGE
};

/**
 this is used to store how ptl is set up and the environment.  It is here as it will be per policy and not 1 per agent.*/
struct TransferConfiguration {
    EnvrionmentChangeType environment;
    SelectionType selection;
    int amountToTransfer;
    MergeType merge;
    int mergeParam; //if adaptive munber of visit max if probablistic param% of time accept
    int currentShadowEvaluationTime;

};

class Policy {
public:

    Policy(std::string name);
    ~Policy();

    void setPolicyName(std::string policyName);
    std::string getPolicyName() const;
    void setConfig(TransferConfiguration config);
    TransferConfiguration getConfig() const;
    void setSlowChange(bool input);
    void setFastChange(bool input);
    void setSharpChangeHappened(bool input);
    void setNeighbourChanged(bool input);
    void setILearned(bool input);
    void setIChanged(bool input);
    void setSelectionType(SelectionType method, int amountToShare);
    void setMergeType(MergeType method, int adptiveTime);
    void setFastChangeStart(double fastChangeStart);
    double getFastChangeStart() const;
    void setSharpChangeStart(double sharpChangeStart);
    double getSharpChangeStart() const;
    void setEnvironmentChangeThreshold(int environmentChangeThreshold);
    int getEnvironmentChangeThreshold() const;
    int getLearnedThreshold();
    void setLearnedThreshold(int learnedThreshold);
private:
    std::string policyName;
    TransferConfiguration config;
    int learnedThreshold; //as a % of state space
    int environmentChangeThreshold; //as a % of state space
    double sharpChangeStart; //cusum for if a sharp change 
    double fastChangeStart; //cusum for if a fast  change 
};

#endif /* POLICY_H */

