/* 
 * File:   DWLAgent.h
 * Author: Adam
 *
 * Created on July 11, 2012, 9:10 AM
 */
#ifndef DWLAGENT_H
#define DWLAGENT_H

#include "Policy.h"
#include "Reward.h"
#include "WLearningProcess.h"
#include "CollaborationProcess.h"
#include <string>
#include <memory>
#include "set"
#include "TransferMapping.h"
#include "WTable.h"

class Compare {
public:

    bool operator()(std::string s1, std::string s2) {
        if (s1.compare(s2) < 0)
            return true;
        else
            return false;
    }
};

/**
 * This class will extend agent and provide what is needed for a domain free DWL agent.  It will have to be extended to a particular appliction.
 */
class DWLAgent {
public:
    DWLAgent(std::string ID);
    ~DWLAgent();
    std::string nominate();
    std::string getName();
    void updateLocal(std::string stateName);
    void updateLocal(std::string stateName, double rewardValueIn);
    void updateRemote();
    std::shared_ptr<Policy> addLocalPolicy(std::string name, std::shared_ptr<Reward> rewardIn);
    std::shared_ptr<Policy> addLocalPolicy(std::string name, std::shared_ptr<Reward> rewardIn, TransferConfiguration config, int learnedThold, int enviroThold, int startSharp, int startFast);
    std::vector<std::shared_ptr<WTable>> publishLocalPolicies();
    std::string publishLocalPolicies(std::string path, std::string tag);
    std::set<std::string, Compare> getNeighbours()const;
    void addRemotePolicy(std::string path); //place holder
    void addRemotePolicy(std::string name, WTable dopIn); //place holder
    void addRemotePolicy(std::string name, std::vector<std::shared_ptr<WTable>> dopIn);
    void addRemotePolicy(std::vector<std::pair<std::string, std::shared_ptr<WTable>> > policies);
    void addMessage(std::string name, std::pair<std::string, double> messageIn); //place holder 
    void addMessage(std::pair< std::string, std::pair<std::string, double> >); //place holder 
    void addMessage(std::string messageIn); //place holder 
    std::pair< std::string, std::pair<std::string, double> > writeMessage();
    std::string writeMessage(int);
    void purgeCommsBuffer(); //clear the comms
    void finishRun(); //convinience method learn from c etc
    void purgeSugestedActionsBuffer(); //clear the comms
    void createColaberationPolicy(std::string name, int numberOfvalues, double maxValue);
    std::string getName() const;
    void writePolicies(std::string name, std::string tag); //put out file for the policies
    int numberOfLocalPolicies();
    int numberOfRemotePolicies();
    std::shared_ptr<QTable> replaceActionsInRemoteStateSpace(std::shared_ptr<WTable> wIn);
    void readInComms(std::vector< std::pair< std::string, std::pair<std::string, double> > > input);
    void readPolicies(std::string name);
    void addNeighbours(std::string name); //place holder change parameter when i no what gridlab does
    int numberOfNeighbours();
    void setUsingTransferLearning(bool usingTransferLearning);
    bool getUsingTransferLearning() const;
    void addMapping(std::string sourceNameIn, std::string targetNameIn, std::shared_ptr<QTable> sourceIn, std::shared_ptr<QTable> targetIn, bool populateRandomly);
    std::vector<std::pair<std::string, std::pair<std::string, double> > > transferToAllFromOne(std::string policyName);
    std::string transferToAllFromAll();
    std::vector<std::pair<std::string, std::pair<std::string, double> > > findInterestingPair(std::string policyName);
    std::vector<std::pair<std::string, double> > findInterestingW(std::string policyName);
    void findAllInterestingPairs(std::vector < std::pair<std::string, std::pair<std::string, double> > >& input);
    void findAllInterestingWs(std::vector < std::pair<std::string, std::pair<std::string, double> > >& input);
    std::pair<std::string, std::pair<std::string, double> > findUninterestingPair(std::string policyName);
    void readTransferedInfoIn(std::vector<std::pair<std::string, std::pair<std::string, double> > > input);
    void readTransferedInfoIn(std::string input);
    void readTransferedWInfoIn(std::vector<std::pair<std::string, std::pair<std::string, double> > > input);
    int getNumberOfMappings();
    double getHistoricReward();
    void manageLearning(bool learnQ, bool learnW);
    void changeActionSelectionTemperature(int input);
    void changeAlphaGamma(double alpha, double gamma);
    void changeEGreadyE(double input);
    void chooseActionSelectionMethod(bool boltzmann, bool eGready, bool taylor);
    std::vector<std::shared_ptr<QTable>> getLocalQTables();
    void printReward(std::string input, std::string tag);
    void addMapping(std::string sourceNameIn, std::string targetNameIn, std::shared_ptr<WTable> sourceIn, std::shared_ptr<WTable> targetIn);
    void printMappings(std::string filename, int mode, std::string tag);
    void updateLearnedMappingFromTarget();
    void updateLearnedMappingFromSource(std::string stateToUpdate);
    std::string sendFeedback();
    void recieveFeedback(std::string input);
    void loadMapping(std::string sourceNameIn, std::string targetNameIn, std::string address);
    std::string transferToOneFromAll(std::string agentName);
    void recieveMixedComms(std::string input);
    void updateLearnedMappingFromAnts();
    void addMapping(std::string sourceNameIn, std::string targetNameIn, std::shared_ptr<QTable> sourceIn, std::shared_ptr<QTable> targetIn, std::shared_ptr<Reward> sourceRewardIn, std::shared_ptr<Reward> targetRewardIn);
    void adaptAndReconfigure();
    void setConfigParams(TransferConfiguration tc, int learnedThold, int enviroThold, int startSharp, int startFast);
    int getLearnedThreshold();
    void clearReward();
    void addSearchMapping(std::string sourceNameIn, std::string targetNameIn, std::shared_ptr<QTable> sourceIn, std::shared_ptr<QTable> targetIn);
    void printCurrentStates();
    std::vector<std::string> getLocalPolicyNames();
protected:
    //a nice over view of smart pointers is here http://www.umich.edu/~eecs381/handouts/C++11_smart_ptrs.pdf
    std::vector<std::shared_ptr<Policy>> localPolicies; //this is the policies this agent has regardless of neighbours
    std::vector<std::shared_ptr<Policy>> remotePolicies; //these are the policies gathered from neighbours
    std::set<std::string, Compare> neighbours; //are the agetns neighbours in the dwl sense (not transfer)
    std::shared_ptr<CollaborationProcess> collaborationPolicy; //this is used to learn the colaberation coefficent
    std::vector< std::pair< std::string, std::pair<std::string, double> > > sugestedActions; //this holds the action proposed by each policy and the policy, prior to a winner being picked.  The KeyValue pair is currentState, sugestedAction and the W value for them.
    std::string previousWinner; //used to update the learning values
    std::vector< std::pair< std::string, std::pair<std::string, double> > > communications; //this will store the information recieved from neigbours
    std::string name; //shouldnt have spaces as this causes file name problems
    double totalReward; //total this time used for colaberation
    double historicReward; //total ever
    std::vector<std::string> rewardLog; //use string so can put in adapt notes
    double calculatedRewardForPreviousAction; //store the reward that was got for an action so it can be communicated to neighbours
    //transfer stuff
    bool usingTransferLearning;
    std::vector<std::shared_ptr<TransferMapping>> mappings;
    std::vector<std::shared_ptr<TransferMapping>> wMappings;
    bool learningQ;
    bool learningW;
    std::string winningAction; //the action that was used for internal use only
    int shadowTranferCount; //-1 indicates we are working on the normal copy 0 or positive we are on a shadow copy 

}; //TODO change what the updates actually mean ie drop actions make general

#endif /* DWLAGENT_H */

