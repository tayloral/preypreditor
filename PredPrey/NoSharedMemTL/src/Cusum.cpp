/* 
 * File:   Cusum.cpp
 * Author: Adam
 * 
 * Created on 05 May 2015, 11:16
 */

#include "Cusum.h"
#include <cmath>
#include <vector>
#include <numeric>
#include <algorithm>
#include <functional>
#include <cmath>
#include <iostream>

Cusum::Cusum()
{
    lastOutput = 0;
    modelTime = 100; //to build model
    mean = nan("");
    stdDev = nan("");
    h = 0;
    k = 0;
    alpha = nan("");
    beta = nan("");
    delta = nan("");
    shi = 0;
    slo = 0;
    sampleCount = 0;
    modelBuilt = false;
    wasOutOfRange = false;
}

bool Cusum::getWasOutOfRange()
{
    return wasOutOfRange;
}

Cusum::Cusum(double a, double b, double d)
{
    lastOutput = 0;
    modelTime = 100; //to build model
    mean = nan("");
    stdDev = nan("");
    h = 0;
    k = 0;
    SetAlpha(a);
    SetBeta(b);
    SetDelta(d);
    beta = nan("");
    delta = nan("");
    shi = 0;
    slo = 0;
    sampleCount = 0;
    modelBuilt = false;
    wasOutOfRange = false;
}

/**
 * change the model set and clear the model (h&k need to be re calculated)
 */
void Cusum::resetModel()
{
    modelBuilt = false;
    modelSamples.clear();
    modelSamples.shrink_to_fit(); //save space for a while
    mean = nan("");
    stdDev = nan("");
    sampleCount = 0;
    lastOutput = 0; //prevent accidental re-trigerring of adaption
    h = 0;
    k = 0;
    shi = 0;
    slo = 0;
    wasOutOfRange = false;
}

/**
 * gives a new copy of this one in seperate memory with no model
 * @return the new one
 */
std::shared_ptr<Cusum> Cusum::deepCopy()
{
    std::shared_ptr<Cusum> newOne = std::make_shared<Cusum>();
    newOne->SetAlpha(this->GetAlpha());
    newOne->SetDelta(this->GetDelta());
    newOne->SetBeta(this->GetBeta());
    newOne->SetModelBuilt(false);

    return newOne;
}

void Cusum::SetDelta(double delta)
{
    this->delta = delta;
    if ((std::isnan(alpha) == false)&&(std::isnan(beta) == false))
    {//others are set 
        this->calculateHAndK();
        // std::cerr << "in set delta\n";
    }
}

double Cusum::GetDelta() const
{
    return delta;
}

void Cusum::SetBeta(double beta)
{
    this->beta = beta;
    if ((std::isnan(alpha) == false)&&(std::isnan(delta) == false))
    {//others are set 
        this->calculateHAndK();
        //std::cerr << "in set beta\n";
    }
}

double Cusum::GetBeta() const
{
    return beta;
}

void Cusum::SetAlpha(double alpha)
{
    this->alpha = alpha;
    if ((std::isnan(delta) == false)&&(std::isnan(beta) == false))
    {//others are set 
        this->calculateHAndK();
        // std::cerr << "in set alpha\n";
    }
}

double Cusum::GetAlpha() const
{
    return alpha;
}

double Cusum::GetK() const
{
    return k;
}

double Cusum::GetH() const
{
    return h;
}

Cusum::~Cusum()
{
    modelSamples.clear(); //empty
    modelSamples.shrink_to_fit(); //now reduce mem
}

void Cusum::SetModelBuilt(bool modelBuilt)
{
    this->modelBuilt = modelBuilt;
}

bool Cusum::IsModelBuilt() const
{
    return modelBuilt;
}

void Cusum::SetSampleCount(int sampleCount)
{
    this->sampleCount = sampleCount;
}

int Cusum::GetSampleCount() const
{
    return sampleCount;
}

void Cusum::SetStdDev(double stdDev)
{
    this->stdDev = stdDev;
    if (!std::isnan(mean))
    {//if other one is a number
        this->modelBuilt = true;
        this->calculateHAndK();
    }
}

double Cusum::GetStdDev() const
{
    return stdDev;
}

void Cusum::SetMean(double mean)
{
    this->mean = mean;
    if (!std::isnan(stdDev))
    {//if other one is a number
        this->modelBuilt = true;
        this->calculateHAndK();
    }
}

double Cusum::GetMean() const
{
    return mean;
}

/***
 turn a b d into h and k id std dev =0 h&k will be 0 making detector v sensative
 *via Montgomery, D. C. (2000). Introduction to Statistical Quality Control, 4th ed., Wiley, New York, NY.
 */
void Cusum::calculateHAndK()
{
    if ((std::isnan(delta) == false)&&(std::isnan(beta) == false)&&(std::isnan(alpha) == false) && modelBuilt)
    {
        //std::cerr << "calcing h&k\n";
        k = (delta * stdDev) / 2;
        double ln = log((1 - beta) / alpha);
        double d = (2 / (delta * delta)) * ln;
        h = d*k;
        // std::cerr << "a " << alpha << " b " << beta << " delta " << delta << " ln " << ln << " d " << d << " h " << h * 1000 << " k " << k * 1000 << "\n";
    }
    else
    {
        // std::cerr << "In Cusum calk h&k, dont calk without setting a b &d mean and std dev\n";
    }
}

double Cusum::GetSlo() const
{
    return slo;
}

double Cusum::GetShi() const
{
    return shi;
}

int Cusum::getLastOutput() const
{
    return lastOutput;
}

/***
 *http://www.itl.nist.gov/div898/handbook/pmc/section3/pmc323.htm
 *returns 1 for higher uncotroled -1 for lower and 0 for control
 */
int Cusum::calculateEnvironmnetState(double sample)
{
    this->sampleCount++;
    sample = std::abs(sample); //cusum not defined for -ive nunmberes
    if (this->modelBuilt == false)
    {//if not built construct it not validate against it
        if (sampleCount == this->modelTime)
        {//if have enough build model
            // std::cerr << "building model with final sample of " << sample << "\n";
            double sum = std::accumulate(modelSamples.begin(), modelSamples.end(), 0.0);
            mean = sum / modelSamples.size();
            std::vector<double> diff(modelSamples.size());
            std::transform(modelSamples.begin(), modelSamples.end(), diff.begin(), std::bind2nd(std::minus<double>(), mean));
            double sq_sum = std::inner_product(diff.begin(), diff.end(), diff.begin(), 0.0);
            stdDev = std::sqrt(sq_sum / modelSamples.size());
            modelBuilt = true;
            calculateHAndK();
            //std::cerr << " model built"  << " mean= " << mean << " stdDev= " << stdDev << "\n";
        }
        else
        {
            // std::cerr << "adding to model\n";
            modelSamples.push_back(sample);
        }
        return 0; //assume ok when no model
    }

    // std::cerr << "normal operation h&k=" << (double) h << " " << k << "\n";
    double shiNew = shi; //shi t -1
    double sloNew = slo; //slo t -1
    shiNew = shiNew + sample - mean - k;
    sloNew = sloNew - sample + mean - k;

    shi = std::max(0.0, shiNew);
    slo = std::max(0.0, sloNew);
    if (shi > h)
    {
        lastOutput = 1;
        //std::cerr << "normal operation h&shi " << (double)h << " " << shi << "\n";
        //	std::cerr << "1 model  sample of " << sample <<" mean= "<<mean<<" stdDev= "<<stdDev<< "\n";
        // std::cerr << "returning 1" << "\n";
        wasOutOfRange = true;
        return 1;
    }
    else if (slo > h)
    {
        lastOutput = -1;
        //	std::cerr << "normal operation h&slo " << (double)h << " " << slo << "\n";
        //	std::cerr << "-1 model  sample of " << sample << " mean= " << mean << " stdDev= " << stdDev << "\n";
        //std::cerr << "returning -1" << "\n";
        wasOutOfRange = true;
        return -1;
    }
    else
    {
        lastOutput = 0;
        return 0;
    }


}