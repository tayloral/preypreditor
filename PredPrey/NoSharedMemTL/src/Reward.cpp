#include "Reward.h"
#include <iostream>

Reward::Reward()
{
    max_reward = -1;
    reward = 0;
}

Reward::~Reward()
{
}

void Reward::setReward(Reward_Value r_v)
{
    reward = r_v;
    //std::cout << "Reward- reward set to " << r_v << "\n";
}

Reward_Value Reward::getReward()
{
    return reward;
}
