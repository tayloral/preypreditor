/*
 * File:   PreditorAgent.cpp
 * Author: Adam
 *
 * Created on 31 May 2016, 14:30
 */

#include "PreditorAgent.h"
#include "PreditorCoordReward.h"
#include "Constants.h"
#include <sstream>
#include <iostream>
#include <string>
#include <cmath>
#include "NoSharedMemTL/include/Policy.h"

PreditorAgent::PreditorAgent(std::string ID, int width, int height, int visionRange, int neighbourhoodType) : DWLAgent(ID)
{
  double alpha = 0.5;
  double gamma = 0.6;
  //1 for von neumann 2 for moore neighbourhood
  if (USE_OWN_COORD_POLICY)
    {
      //make a reward function so that we can attach it to the agent
      pCReward = std::make_shared<PreditorCoordReward>();

      //create a state space and reward container (everything we need for Q-Learning on its own)
      std::shared_ptr<Policy> predCoordPolicy = this->addLocalPolicy("PredCoord", pCReward); //this sets up everything we need to do Q-learning and attaches it to the agent, giving us a pointer for further customisation
      this->manageLearning(true, true); //set the agent as Q-Learning and W

      for (int a = 1; a <= width; a++)
        {//the x-coord
          for (int b = 1; b <= height; b++)
            {//the y-coord
              std::stringstream ss;
              ss << a << "-" << b; //format the state exactly the same as what the maze gives
              for (int c = 0; c < NUMBER_OF_MOORE_ACTIONS; c++)
                {//add the cant see prey or pred
                  //put them into the state space
                  std::static_pointer_cast<WLearningProcess>(predCoordPolicy)->addStateAction(ss.str(), ACTIONS_STRINGS[c]); //the casting of the policy is so it fits into the interfaces for other things the library can do
                }
            }

        }


      //set the learning parmeters we have in the tutorial
      std::static_pointer_cast<WLearningProcess>(predCoordPolicy)->setQAlpha(alpha);
      std::static_pointer_cast<WLearningProcess>(predCoordPolicy)->setQGamma(gamma);
      this->chooseActionSelectionMethod(false, false, true); //set how we select actions
      std::static_pointer_cast<WLearningProcess>(predCoordPolicy)->setTaylorTemperature(EXPLORE_LENGTH); //setting high numbers make the agent explore more
      std::static_pointer_cast<WLearningProcess>(predCoordPolicy)->printStateSpace("PredCoordConstructor", "test");
    }
  if (USE_VECTOR_POLICY)
    {
      //make a reward function so that we can attach it to the agent
      pRReward = std::make_shared<PreditorVectorReward>();

      //create a state space and reward container (everything we need for Q-Learning on its own)
      std::shared_ptr<Policy> predRangePolicy = this->addLocalPolicy("PredRange", pRReward); //this sets up everything we need to do Q-learning and attaches it to the agent, giving us a pointer for further customisation
      this->manageLearning(true, true); //set the agent as Q-Learning and W

      for (int a = 1; a <= std::ceil(std::sqrt(2 * std::pow(visionRange, 2))); a++)
        {//the magnitude of the vector
          for (int d = 1; d <= std::ceil(std::sqrt(2 * std::pow(visionRange, 2))); d++)
            {//the magnitude of the vector
              for (int b = 0; b <= 180 / BIN_SIZE_DEGREES; b++)
                {//the angle of the vector
                  std::stringstream ss;
                  ss << a << "_" << d << "_" << b; //format the state
                  for (int c = 0; c < NUMBER_OF_MOORE_ACTIONS; c++)
                    {//for the actions
                      //put them into the state space
                      std::static_pointer_cast<WLearningProcess>(predRangePolicy)->addStateAction(ss.str(), ACTIONS_STRINGS[c]); //the casting of the policy is so it fits into the interfaces for other things the library can do
                    }
                }
            }
        }
      for (int c = 0; c < NUMBER_OF_MOORE_ACTIONS; c++)
        {//for the actions
          //put them into the state space
          std::static_pointer_cast<WLearningProcess>(predRangePolicy)->addStateAction("0_0_0", ACTIONS_STRINGS[c]); //the casting of the policy is so it fits into the interfaces for other things the library can do
        }
      //set the learning parmeters we have in the tutorial
      std::static_pointer_cast<WLearningProcess>(predRangePolicy)->setQAlpha(alpha);
      std::static_pointer_cast<WLearningProcess>(predRangePolicy)->setQGamma(gamma);
      this->chooseActionSelectionMethod(false, false, true); //set how we select actions
      std::static_pointer_cast<WLearningProcess>(predRangePolicy)->setTaylorTemperature(EXPLORE_LENGTH); //setting high numbers make the agent explore more
      std::static_pointer_cast<WLearningProcess>(predRangePolicy)->printStateSpace("PredRewardConstructor", "test");
    }
  if (USE_VISION_POLICY)
    {
      //make a reward function so that we can attach it to the agent
      pVReward = std::make_shared<PreditorVisionReward>();
      //create a state space and reward container (everything we need for Q-Learning on its own)
      std::shared_ptr<Policy> predVisionPolicy = this->addLocalPolicy("PredVision", pVReward); //this sets up everything we need to do Q-learning and attaches it to the agent, giving us a pointer for further customisation
      this->manageLearning(true, true); //set the agent as Q-Learning and W

      if (MULTI_FEATURE_VISION)
        {//doesnt matter how far can see only has 4 possibility for state
          visionRange = 4;
        }
      else
        {
          visionRange = 1;
        }

      for (int a = 0; a <= visionRange; a++)
        {
          for (int b = 0; b <= visionRange; b++)
            {
              for (int c = 0; c <= visionRange; c++)
                {
                  for (int d = 0; d <= visionRange; d++)
                    {
                      std::stringstream ss;
                      ss << "U" << a << "-D" << b << "-L" << c << "-R" << d; //format the state exactly the same as what the maze gives
                      for (int e = 0; e < 5; e++)
                        {//for the actions
                          //put them into the state space
                          std::static_pointer_cast<WLearningProcess>(predVisionPolicy)->addStateAction(ss.str(), ACTIONS_STRINGS[e]); //the casting of the policy is so it fits into the interfaces for other things the library can do
                        }
                    }
                }
            }
        }

      //set the learning parmeters we have in the tutorial
      std::static_pointer_cast<WLearningProcess>(predVisionPolicy)->setQAlpha(alpha);
      std::static_pointer_cast<WLearningProcess>(predVisionPolicy)->setQGamma(gamma);
      this->chooseActionSelectionMethod(false, false, true); //set how we select actions
      std::static_pointer_cast<WLearningProcess>(predVisionPolicy)->setTaylorTemperature(EXPLORE_LENGTH); //setting high numbers make the agent explore more
    }
  TransferConfiguration tc;
  /*conf1
  tc.merge = MergeType::adaptiveProbabilistic;
  tc.mergeParam = 60;
  tc.selection = SelectionType::greatestChange;
  tc.amountToTransfer = 4;
   */
  /*conf2
  tc.merge = MergeType::adaptiveProbabilistic;
  tc.mergeParam = 60;
  tc.selection = SelectionType::randomSet;
  tc.amountToTransfer = 4;*/
  /*conf3
  tc.merge = MergeType::onlyTarget;
  tc.mergeParam = 60;
  tc.selection = SelectionType::randomSet;
  tc.amountToTransfer = 4;
   */
  /*conf4*/
  tc.merge = MergeType::adaptive;
  tc.mergeParam = 10;
  tc.selection = SelectionType::manyVisit;
  tc.amountToTransfer = 2;
  /*conf5
  tc.merge = MergeType::adaptive;
  tc.mergeParam = 5;
  tc.selection = SelectionType::greatestChange;
  tc.amountToTransfer = 10;
   */
  tc.environment.fastChange = false;
  tc.environment.iChanged = false;
  tc.environment.iLearned = false;
  tc.environment.neighbourChange = false;
  tc.environment.processedILearned = false;
  tc.environment.processedSharpChnageHappened = false;
  tc.environment.slowChange = false;
  tc.currentShadowEvaluationTime = 40;
  this->setConfigParams(tc, 50, 30, 180, 40);
  this->setUsingTransferLearning(usingPTL);
  std::cout << "Made Pred with= " << this->numberOfLocalPolicies() << " policies\n";
}

PreditorAgent::~PreditorAgent() { }

void PreditorAgent::setMoveResult(MOVE_RESULTS moveResultIn)
{
  if (USE_OWN_COORD_POLICY)
    {
      pCReward->setMoveResult(moveResultIn);
    }
  if (USE_VISION_POLICY)
    {
      pVReward->setMoveResult(moveResultIn);
    }
  if (USE_VECTOR_POLICY)
    {
      pRReward->setMoveResult(moveResultIn);
    }
  //std::cout << "PreditorAgent::setMoveResult\n";
}

void PreditorAgent::updateLocal(std::string stateName)
{
  if (shadowTranferCount >= 0)
    {//if shouold be counting
      shadowTranferCount++;
      //  std::cout << "counting\n";
    }
  std::stringstream strs;
  strs << historicReward;
  rewardLog.push_back(strs.str());
  //
  if (stateName.length() <= 0)
    {
      //do nothing

      std::cerr << this->getName() << "'s statename is too short in update local  " << "\n";
      std::cerr << stateName << "=statename " << remotePolicies.size() << "\n";
    }
  else
    {
      std::vector < std::shared_ptr < Policy>>::iterator localIterator = localPolicies.begin();
      while (localIterator != localPolicies.end())
        {
          std::shared_ptr<Policy> toCast = *localIterator;
          std::shared_ptr<WLearningProcess> test = std::static_pointer_cast<WLearningProcess>(toCast);
          if (test->getPolicyName() == previousWinner)
            {//if this one won tell it to update and learn
              std::string parsedStateName = "not dark yet";

              /*state used to change pased on num policies now always coord-vision-vector
               * if (this->numberOfLocalPolicies() == 1 && test->getPolicyName().compare(this->getName()) == 0)
              {//if a single policy no need to parse
                  parsedStateName = stateName.substr(stateName.find("~") + 1, stateName.length() - stateName.find("~"));
              }
              else
               */ if (test->getPolicyName().compare("PredVision") == 0)
                {//is a pred vision
                  parsedStateName = stateName.substr(stateName.find("~") + 1, stateName.find_last_of("~") - (stateName.find("~") + 1));
                }
              else if (test->getPolicyName().compare("PredCoord") == 0)
                {//is a pred coord
                  parsedStateName = stateName.substr(0, stateName.find("~"));
                }
              else if (test->getPolicyName().compare("PredRange") == 0)
                {
                  parsedStateName = stateName.substr(stateName.find_last_of("~") + 1, stateName.length() - stateName.find_last_of("~"));
                }
              else
                {
                  std::cout << "1 " << stateName << " and pol= |" << test->getPolicyName() << "| " << parsedStateName << "\n";
                }

              test->update(parsedStateName, 0, true); //doesnt matter what reward is passed as local policies can self-calculate
              if (sugestedActions.size() > 0)
                {//if there is a publiched action
                  std::vector< std::pair< std::string, std::pair<std::string, double> > >::iterator sugestedIterator = sugestedActions.begin();
                  while ((*sugestedIterator).first != this->previousWinner)
                    {//find the winner in comms vector
                      sugestedIterator++;
                    }
                  //now we have the pointer to its update put in the reward
                  (*sugestedIterator).second.second = test->getMostRecentReward();
                }
              historicReward += test->getMostRecentReward(); //as this won accumulate its reward
              if (usingTransferLearning)
                {
                  //std::cerr << "about to make feedback\n";
                  if (test->getMostRecentReward() > 0)
                    {//if good reward feedback good
                      /*
                       //old update for if pos reward good was nieve if (usingTransferLearning)
                       {
                       std::stringstream ss;
                       ss << test->getPreviousState() << ":" << test->getCurrentAction();

                       test->provideTransferFeedback(ss.str(), true);
                       }*/
                    }
                  else
                    {//bad feeback
                      /*if (usingTransferLearning)
                      {
                      std::stringstream ss;
                      ss << test->getPreviousState() << ":" << test->getCurrentAction();

                      test->provideTransferFeedback(ss.str(), false);
                      }*/
                    }
                }
            }
          else
            {//it lost learn not nothing
              std::string parsedStateName = "not dark yet";
              /*state used to change pased on num policies now always coord-vision-vector
               * if (this->numberOfLocalPolicies() == 1 && test->getPolicyName().compare(this->getName()) == 0)
              {//if a single policy no need to parse
                  parsedStateName = stateName.substr(stateName.find("~") + 1, stateName.length() - stateName.find("~"));
              }
              else
               */ if (test->getPolicyName().compare("PredVision") == 0)
                {//is a pred vision
                  parsedStateName = stateName.substr(stateName.find("~") + 1, stateName.find_last_of("~") - (stateName.find("~") + 1));
                }
              else if (test->getPolicyName().compare("PredCoord") == 0)
                {//is a pred coord
                  parsedStateName = stateName.substr(0, stateName.find("~"));
                }
              else if (test->getPolicyName().compare("PredRange") == 0)
                {
                  parsedStateName = stateName.substr(stateName.find_last_of("~") + 1, stateName.length() - stateName.find_last_of("~"));
                }
              else
                {
                  std::cout << "2 " << stateName << " and pol= |" << test->getPolicyName() << "| " << parsedStateName << "\n";
                }
              test->update(parsedStateName, 0, false); //doesnt matter what reward is passed as local policies can self-calculate
              if (usingTransferLearning)
                {
                  //std::cerr << "about to make feedback\n";
                  if (test->getMostRecentReward() > 0)
                    {//if good reward feedback good
                      std::stringstream ss;
                      ss << test->getPreviousState() << ":" << test->getCurrentAction();
                      test->provideTransferFeedback(ss.str(), true);
                    }
                  else
                    {//bad feeback
                      std::stringstream ss;
                      ss << test->getPreviousState() << ":" << test->getCurrentAction();
                      test->provideTransferFeedback(ss.str(), false);
                    }
                }
            }

          localIterator++;
        }
    }
}
