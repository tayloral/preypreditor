/* 
 * File:   PreditorAgent.h
 * Author: Adam
 *
 * Created on 31 May 2016, 14:30
 */
#include "DWLAgent.h"
#include "PreditorCoordReward.h"
#include "PreditorVectorReward.h"
#include "PreditorVisionReward.h"
#ifndef PREDITORAGENT_H
#define PREDITORAGENT_H

class PreditorAgent : public DWLAgent {
public:
    PreditorAgent(std::string ID, int width, int height, int visionRange, int neighbourhoodType);
    virtual ~PreditorAgent();
    void setMoveResult(MOVE_RESULTS moveResultIn);
    void updateLocal(std::string stateName);
private:
    MOVE_RESULTS moveResult;
    std::shared_ptr<PreditorCoordReward> pCReward;
    std::shared_ptr<PreditorVisionReward> pVReward;
    std::shared_ptr<PreditorVectorReward> pRReward;
};

#endif /* PREDITORAGENT_H */

