%plot catch curves for one experiment%test if x is greater than y
clear;
expName=sprintf('PTLFinalTestRL');
numberOfExperiments=10;
folder=sprintf('C:\\Users\\Adam\\Documents\\NetBeansProjects\\repos\\PredPrey\\backEnd\\PredPrey\\%s\\',expName);
lineNames={'Pred a';'Pred b';'Pred c';'Pred d';'The Other Guy';'The Other Guy 2';'The Other Guy 3';'The Other Guy 4'};
plotStuff=1;%0 or 1
printing=1;%0 or 1
publishing=0;%0 or 1 if 1 can take ~10 mins to render
diferntial=0;%0 for cumulative reward, 1 for diferential
titleText=sprintf('Testing Catches');

if plotStuff==1||printing==1
    fig1=figure();
end
lineStyle={'-.'; '-'; '--'; '-.'};
lineWeight={.5;.5;.5;.5;.5;.5;.5;.5};
lineColour={'r';[0 0.7 0];'b';'k'};
lineMarker={'x';'.';'*';'o'};
lineTypes={'-b';'-r';'-k';'-g';'-m';'--b';'--r';'--g'};
plotStyleIndex=1;
hold on
files=dir(fullfile(folder,'*-preditorCatches*.stats'));
files = {files.name}';
%make a matrix of files base on same pred
sizeOfFiles=size(files);
files=reshape(files,sizeOfFiles(1)/numberOfExperiments,numberOfExperiments);
sizeOfFiles=size(files);
%get the average and stderr
for a=1:sizeOfFiles(1)
    for b=1:sizeOfFiles(2)
        file=files(a,b);
        filename=sprintf('%s%s',folder,files{a,b});
        data=csvread(filename);
        data(end)=[];
        if diferntial==0
            data=cumsum(data);
        end
        allData(a,b,:)=data;
    end
end%now all the data is in all data
sizeOfAllData=size(allData);
averages=mean(allData,2);%get mean accross run number
stdErr=std(allData,0,2);%get std accross run number
averages=squeeze(averages);
stdErr=squeeze(stdErr);
%plot the preds output on a graph
if plotStuff==1||printing==1
    sizeOfAverages=size(averages);
    for a=1:sizeOfAverages(1)
        %data=transpose(data);
        [l,p]=boundedline(1:sizeOfAverages(2),averages(a,:),stdErr(a,:),cell2mat(lineTypes(plotStyleIndex)),'alpha','transparency', 0.1);%,'linestyle',cell2mat(lineStyle(plotStyleIndex,1)),'color',cell2mat(lineColour(plotStyleIndex,1)),'LineWidth',cell2mat(lineWeight(plotStyleIndex,1)),'DisplayName',file);
        outlinebounds(l,p);
        plotStyleIndex=plotStyleIndex+1;
        if plotStyleIndex>length(lineTypes)
            plotStyleIndex=1;
        end
    end
end
plotStyleIndex=1;
if plotStuff==1||printing==1
    %set graph area
    kids = get(fig1, 'Children');
    kids = get(kids, 'Children');
    kidsTypes=get(kids,'Type');
    seenLine=false;
    for a=1:size(kids)%go through all children of axis and see if there is a line finish the styling
        if seenLine==true&&strcmp(kidsTypes(a),'line')% is a line
            %,'linestyle',cell2mat(lineStyle(plotStyleIndex,1)),'color',cell2mat(lineColour(plotStyleIndex,1)),'LineWidth',cell2mat(lineWeight(plotStyleIndex,1)),'DisplayName',file);
            set(kids(a),'LineWidth',cell2mat(lineWeight(plotStyleIndex,1)));
            set(kids(a),'DisplayName',cell2mat(lineNames((sizeOfFiles(1)+1)-plotStyleIndex)));%must reverse names as we get them in opp order from figure
            plotStyleIndex=plotStyleIndex+1;
            if plotStyleIndex>length(lineTypes)
                plotStyleIndex=1;
            end
            seenLine=false;
        elseif strcmp(kidsTypes(a),'line')% is a patch
            seenLine=true;%reset so we can change width again
            set(get(get(kids(a),'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
        else
            set(get(get(kids(a),'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
        end
    end
    box on
    set(gca,'FontSize',10)
    leg_end=legend(gca, 'show','Location','northwest');
    set(leg_end,'color','none');
    title(titleText)
    xlabel('Time Step') % x-axis label
    %axisSpace=(stop-start)/(axisCount-1);%
    % set(gca,'xtick',0:axisSpace:(stop-start))%change axis
    %set(gca,'XTickLabel',dates(start:axisSpace:stop));
    %rotateXLabels( gca(), 90 );
    ylabel('Total Catches') % y-axis label
    %set(gca,'ytick',[])%change axis
    %xlim(min, max)
    ymin=min(averages-stdErr);
    ymin=min(ymin);
    ymax=max(averages+stdErr);
    ymax=max(ymax);
    ylim([ ymin ymax])
end
if printing==1
    if plotStuff==0
        set(fig1, 'visible','off');
    end
    %           a4
    paperX=7.5;%6.25
    paperY=6.25;%7.5
     outname=sprintf('%s%s-Catches.tikz',folder,expName);
    set(gcf, 'color', 'none');
    set(gca, 'color', 'none');
%     set(gcf, 'PaperSize', [paperX paperY]);
%     set(gcf, 'PaperPositionMode', 'manual');
%     set(gcf, 'PaperPosition', [0 0 paperX paperY]);
%     set(gcf, 'PaperUnits', 'inches');
%     set(gcf, 'PaperSize', [paperX paperY]);
%     set(gcf, 'PaperPositionMode', 'manual');
%     set(gcf, 'PaperPosition', [0 0 paperX paperY]);
%     set(gca(), 'LooseInset', get(gca(), 'TightInset'));
%     if publishing==1
%         quality='-r2500';
%     else
%         quality='-r100';
%     end
   % print(gcf, '-dpdf','-noui',quality,outname)
matlab2tikz(outname, 'height', '\figureheight', 'width', '\figurewidth','showInfo', false);%show info false turns off a  message

end
maxs=max(averages,[],2);
maxsSize=size(maxs);
maxsCount=1;
while maxsCount<maxsSize(1)
    %find max in maxs
    [maxMax,maxMaxIndex] = max(maxs);
    maxs(maxMaxIndex)= log(0);%make -inf
    [maxMax,maxNextMaxIndex] = max(maxs);
    %tail both test if x!=y right test if x>y left test if x<y
    %[h,p,ci,stats] null is that they are the same h=1 test is rejected 0 accepeted
    h =ttest2(allData(maxMaxIndex,:,:),allData(maxNextMaxIndex,:,:),'Alpha',0.01,'Tail','right','Vartype','unequal');
    h=squeeze(h);
    sprintf('Bigger way - %s is bigger than %s for the %f%% of the time',cell2mat(lineNames(maxMaxIndex)),cell2mat(lineNames(maxNextMaxIndex)),(nansum(h)/length(h)))
    h =ttest2(allData(maxNextMaxIndex,:,:),allData(maxMaxIndex,:,:),'Alpha',0.01,'Tail','right','Vartype','unequal');
    h=squeeze(h);
    sprintf('Other way - %s is bigger than %s for the %f%% of the time',cell2mat(lineNames(maxMaxIndex)),cell2mat(lineNames(maxNextMaxIndex)),(nansum(h)/length(h)))
    
    maxsCount=1+maxsCount;
end