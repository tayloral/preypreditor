clear;
dataReductionFactor=20;
%load one at a time
expNameRL=   sprintf('RL+test+third3speedShort+pr4v1+py2Line+10');
%ordered as paper
expNamePTL1=   sprintf('PTL+conf4+third3speedShort+pr4v1+py2Line+10');
expNamePTL2=   sprintf('PTL+test+third3speedShort+pr4v1+py2Line+10');
expNamePTL3=   sprintf('PTL+conf2+third3speedShort+pr4v1+py2Line+10');
expNamePTL4=   sprintf('PTL+conf5+third3speedShort+pr4v1+py2Line+10');
expNamePTL5=   sprintf('PTL+conf3+third3speedShort+pr4v1+py2Line+10');
a=1;
folder=sprintf('C:\\Users\\Work-mine\\Documents\\WorkingDir\\PredPrey\\%s\\',expNamePTL5);
loadDataName=sprintf('%soutDataLearning.mat',folder);
load(loadDataName);
outDataSize=size(outData);
err41(a,:)=outData(outDataSize(2)).c;
ave41(a,:)=outData(outDataSize(2)).b;
a=a+1;
folder=sprintf('C:\\Users\\Work-mine\\Documents\\WorkingDir\\PredPrey\\%s\\',expNamePTL1);
loadDataName=sprintf('%soutDataLearning.mat',folder);
load(loadDataName);
outDataSize=size(outData);
err41(a,:)=outData(outDataSize(2)).c;
ave41(a,:)=outData(outDataSize(2)).b;
a=a+1;
folder=sprintf('C:\\Users\\Work-mine\\Documents\\WorkingDir\\PredPrey\\%s\\',expNamePTL2);
loadDataName=sprintf('%soutDataLearning.mat',folder);
load(loadDataName);
outDataSize=size(outData);
err41(a,:)=outData(outDataSize(2)).c;
ave41(a,:)=outData(outDataSize(2)).b;
a=a+1;
folder=sprintf('C:\\Users\\Work-mine\\Documents\\WorkingDir\\PredPrey\\%s\\',expNamePTL3);
loadDataName=sprintf('%soutDataLearning.mat',folder);
load(loadDataName);
outDataSize=size(outData);
err41(a,:)=outData(outDataSize(2)).c;
ave41(a,:)=outData(outDataSize(2)).b;
a=a+1;
folder=sprintf('C:\\Users\\Work-mine\\Documents\\WorkingDir\\PredPrey\\%s\\',expNamePTL4);
loadDataName=sprintf('%soutDataLearning.mat',folder);
load(loadDataName);
outDataSize=size(outData);
err41(a,:)=outData(outDataSize(2)).c;
ave41(a,:)=outData(outDataSize(2)).b;
a=a+1;
folder=sprintf('C:\\Users\\Work-mine\\Documents\\WorkingDir\\PredPrey\\%s\\',expNameRL);
loadDataName=sprintf('%soutDataLearning.mat',folder);
load(loadDataName);
outDataSize=size(outData);
err41(a,:)=outData(outDataSize(2)).c;
ave41(a,:)=outData(outDataSize(2)).b;
a=a+1;
ave41=transpose(ave41);
err41=transpose(err41);
numberOfDataItems=size(outData(1).b);
numberOfDataItems=numberOfDataItems(1);
figure();
axisXLabelText=sprintf('Time Steps');
axisYLabelText=sprintf('Preys Caught');
xlim([0  numberOfDataItems])
ymin=ave41(1,:)-err41(1,:);
ymax=ave41(1,:)+err41(1,:);
for currentLine=1:6%find min and max
    for currentElement=1:numberOfDataItems
        ymin=min(ave41(currentElement,currentLine)-err41(currentElement,currentLine),ymin);
        ymax=max(ave41(currentElement,currentLine)+err41(currentElement,currentLine),ymax);
    end
end
ymin=min(ymin);
ymax=max(ymax);
ylim([ymin ymax])
ylabel(axisYLabelText);
xlabel(axisXLabelText);
title('3,000 Steps','FontWeight','Normal')
hold on
thisColour=lines(6);%brighten(lines(6),-.3);
colourMap=[4;6;2;3;1;5];
for a=1:6
    b=colourMap(a);
    [l,p]=boundedline(squeeze(1:dataReductionFactor:numberOfDataItems),squeeze(ave41(1:dataReductionFactor:numberOfDataItems,a)),squeeze(err41(1:dataReductionFactor:numberOfDataItems,a)),'cmap',thisColour(b,:),'alpha','transparency', 0.15);%,'linestyle',cell2mat(lineStyle(plotStyleIndex,1)),'color',cell2mat(lineColour(plotStyleIndex,1)),'LineWidth',cell2mat(lineWeight(plotStyleIndex,1)),'DisplayName',file);
    l.LineWidth=1;
    set(get(get(p,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
end
legend('RL','PTL 1','PTL 2','PTL 3','PTL 4','PTL 5','Location','northwest')
outname=sprintf('C:/Users/Work-mine/Dropbox/reports/JAAMAS/img/ptlLateralParam.tex');
%notworking atemp at adding doc options autoly%matlab2tikz(outname,  'width', '\figurewidth','showInfo', false,'standalone', true);%show info false turns off a  message
%removed font enc from matlab2tiokx
matlab2tikz(outname,  'width', '\figurewidth','showInfo', false,'parseStringsAsMath',true,'standalone', true','extracode',['\pgfplotsset{','max space between ticks=50pt,','try min ticks=3,','yticklabel style={','/pgf/number format/fixed,','/pgf/number format/precision=1','},','scaled y ticks=false,','xticklabel style={','/pgf/number format/fixed,','/pgf/number format/precision=1','},','scaled x ticks=false,','title style={yshift=0pt,}','}']);%show info false turns off a  message