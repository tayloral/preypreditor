%load jmass data for net performance per exp
%names=['RL';'1';'2';'3';'4';'5']
%load one at a time
% expName=   sprintf('PTL+conf5+third3speedShort+pr4v1+py2Line+10');
% folder=sprintf('C:\\Users\\Work-mine\\Documents\\WorkingDir\\PredPrey\\%s\\',expName);
% loadDataName=sprintf('%soutData.mat',folder);
% load(loadDataName);
% outDataSize=size(outData);
% err41(a)=outData(outDataSize(2)).c(3000)
% ave41(a)=outData(outDataSize(2)).b(3000)
% a=a+1;
%load net perf per exp.mat
allData=[ave22;ave32;ave42;ave41];
allErr=[err22;err32;err42;err41];
%in alldata and err as ['RL';'conf 1';'conf 2';'conf 3';'conf 4';'conf 5']
%re order to mathc text
approchOrder=[1;5;2;3;6;4]%rl 4 1 2 5 3
%allData=transpose(allData)
figure();
hold on
delta=.3;
xCoord=[1;4;7;10];
xticklabels({'2 Vs 2','3 Vs 2','4 Vs 2','4 Vs 1'})
xticks(xCoord)
ylim([-10 260])
ylabel('Preys Caught');
xlabel('Predators Vs Preys');
title('3,000 Steps','FontWeight','Normal')
xlim([1-3.5*delta 10+4.5*delta])
xCoord=xCoord-2.5*delta;
marker =['+';'x';'o';'s';'^';'v'];
thisColour=lines(6);
colourMap=[4;6;2;3;1;5];
for a=1:6
    mappedIndex=approchOrder(a);
    b=colourMap(a);
    e=errorbar(xCoord(:),allData(:,mappedIndex),allErr(:,mappedIndex),marker(a),'MarkerSize',7,'MarkerEdgeColor',thisColour(b,:),'MarkerFaceColor',thisColour(b,:))
    e.Color = thisColour(b,:);
    xCoord=xCoord+delta;
end
legend('RL','PTL 1','PTL 2','PTL 3','PTL 4','PTL 5','Location','northwest')
outname=sprintf('C:/Users/Work-mine/Dropbox/reports/JAAMAS/img/ptlParam.tex');
%notworking atemp at adding doc options autoly%matlab2tikz(outname,  'width', '\figurewidth','showInfo', false,'standalone', true);%show info false turns off a  message
%removed font enc from matlab2tiokx
matlab2tikz(outname,  'width', '\figurewidth','showInfo', false,'parseStringsAsMath',true,'standalone', true','extracode',['\pgfplotsset{','max space between ticks=20pt,','try min ticks=3,','yticklabel style={','/pgf/number format/fixed,','/pgf/number format/precision=1','},','scaled y ticks=false,','xticklabel style={','/pgf/number format/fixed,','/pgf/number format/precision=1','},','scaled x ticks=false,','title style={yshift=0pt,}','}']);%show info false turns off a  message