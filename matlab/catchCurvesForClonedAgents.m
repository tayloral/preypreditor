%plot catch values for each cloane test
function functionOutput=plotCloneCatchCurves
clear;
expName=sprintf(    'RL+norm+third3speedShort+pr4v1+py2Line+10');
expNameAlt=sprintf('test4PredTests');
folder=sprintf('C:\\Users\\Work-mine\\Documents\\WorkingDir\\PredPrey\\%s\\',expName);

%control bools
diferntial=0;%0 for cumulative reward, 1 for diferential
plotStuff=1;%0 or 1
printing=0;%0 or 1
sumAgents=1;
outlinesUsed=0;
cloneIndicatior='_test_';
timeEndIndicatior='_EndOfRun-';
preditorCatechesIndicator='preditorCatches-';
%internal functions
    function correctFile = filterFiles(a)%check if a contains the string _test_
        if ~isempty(strfind(a,cloneIndicatior))==1
            correctFile=a;
        else
            correctFile='';
        end
    end
    function cloneTime = extractCloneTimes(a)%get the time value
        arg1=((strfind(a,cloneIndicatior)+length(cloneIndicatior)));
        arg2=((strfind(a,timeEndIndicatior)-1));
        cloneTime=a(arg1:arg2);
    end
    function preditorNames = extractPreditorNames(a)%get the name
        arg1=((strfind(a,preditorCatechesIndicator)+length(preditorCatechesIndicator)));
        arg2=((strfind(a,expName)-2));%as expname has no seperator
        preditorNames=strrep(a(arg1:arg2),'test','');
    end
%check if we already have a saved data file
files=dir(fullfile(folder,'*.mat'));
files = {files.name}';
filesSize=size(files);
if filesSize>0
    %load the saved .mat instead of reading csv
    %prep data
    loadDataName=sprintf('%soutData.mat',folder);
    load(loadDataName);
    dataSize=size(outData);
    numberOfDataItems=size(outData(1).b);
    numberOfDataItems=numberOfDataItems(2);
    dataCounter=1;
    %prep figure
    plotStyleIndex=1;
    if plotStuff==1||printing==1
        fig1=figure();
    end
    titleText=sprintf('Reused Data -Testing Catches: %s',expName);
    title(titleText);
    plotStyleIndex=1;
    hold on
    ymin=min(outData(1).b-outData(1).c);%estimate 1 is lowest max in highest
    ymin=min(ymin);
    ymax=max(outData(dataSize(2)).b+outData(dataSize(2)).c);
    ymax=max(ymax);
    lineTypes={'-r';'-b';'-g';'-k';'-c';'-m';'-y';'--k'};
    while dataCounter<=dataSize(2)
        [l,p]=boundedline(squeeze(1:numberOfDataItems),squeeze(outData(dataCounter).b),squeeze(outData(dataCounter).c),cell2mat(lineTypes(plotStyleIndex)),'alpha','transparency', 0.01);%,'linestyle',cell2mat(lineStyle(plotStyleIndex,1)),'color',cell2mat(lineColour(plotStyleIndex,1)),'LineWidth',cell2mat(lineWeight(plotStyleIndex,1)),'DisplayName',file);
        if outlinesUsed==1
            outline=outlinebounds(l,p);
        end
        %name main line for legend entry
        lineName=sprintf('All agent Training Steps:%d',outData(dataCounter).a);
        set(l,'DisplayName',lineName);
        %turn legend entries off for patch and edge lines
        if outlinesUsed==1
            set(get(get(outline,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
        end
        set(get(get(p,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
        plotStyleIndex=plotStyleIndex+1;
        if plotStyleIndex>length(lineTypes)
            plotStyleIndex=1;
        end
        dataCounter=dataCounter+1;
    end
    leg_end=legend(gca, 'show','Location','northwest');
    set(leg_end,'color','none');
else%read from the csvs
    %do the first folder
    files=dir(fullfile(folder,'*-preditorCatches*.stats'));
    files = {files.name}';
    files = squeeze(cellfun(@(x)filterFiles(x), files, 'UniformOutput', false));
    files =files(~cellfun('isempty',files));
    %get name of last file and extract its number for exp number
    numberOfExperiments=files(length(files));
    numberOfExperiments=numberOfExperiments{1};
    numberOfExperiments=1+str2num(numberOfExperiments(1:(strfind(numberOfExperiments,cloneIndicatior)-1)));
    
    %get all the clone intervals
    cloneTimes=sort(str2double(unique(cellfun(@(x)extractCloneTimes(x), files, 'UniformOutput', false))));
    numberOfTimes=length(cloneTimes);
    %get the preditor names
    preditorNames=char(unique(cellfun(@(x)extractPreditorNames(x), files, 'UniformOutput', false)));
    numberOfAgents=length(preditorNames);
    
    %now we have all of the details to grab data
    currentExperiment=1;
    while currentExperiment<=numberOfExperiments
        currentTime=1;
        while currentTime<=numberOfTimes
            currentAgent=1;
            while currentAgent<=numberOfAgents
                %open the file and get data
                filename=sprintf('%s%04d%s%d%s%s%stest-%s.csv.stats',folder,currentExperiment-1,cloneIndicatior,cloneTimes(currentTime),timeEndIndicatior,preditorCatechesIndicator,preditorNames(currentAgent,:),expName)
                tempData=csvread(filename);
                tempData(end)=[];
                if diferntial==0
                    tempData=cumsum(tempData);
                end
                data(currentExperiment,currentTime,currentAgent,:)=tempData;
                currentAgent=currentAgent+1;
            end
            currentTime=currentTime+1;
        end
        currentExperiment=currentExperiment+1;
    end
    %now we have all the data get it into a plottable form
    if sumAgents==1
        backupData=sum(data,3);%sum accross agent number (third dimetuion)
    else
        backupData=data;
    end
    backupData=squeeze(backupData);
    averages=mean(backupData);%get mean accross run number (first dimetuion)
    stdErr=std(backupData);%get std accross run number
    averages=squeeze(averages);
    stdErr=squeeze(stdErr);
    %cobnfigure plot
    titleText=sprintf('Testing Catches: %s',expName);
    if numberOfAgents==4
        lineNames={'Pred a';'Pred b';'Pred c';'Pred d';'Pred ALT a';'Pred ALT b';'Pred ALT c';'Pred ALT d'};
        lineStyle={'-'; '-'; '-'; '--';'--';'--';'--'};
        lineWeight={.5;.5;.5;.5;.5;.5;.5;.5};
        lineColour={'r';'b';'g';'k';'c';'m';'r';'b'};
        lineMarker={'x';'.';'*';'o'};
        lineTypes={'-r';'-b';'-g';'-k';'-c';'-m';'-y';'--k'};
    else if numberOfAgents==3
            lineNames={'Pred a';'Pred b';'Pred c';'Pred ALT a';'Pred ALT b';'Pred ALT c';'The Other Guy 3';'The Other Guy 4'};
            lineStyle={'-'; '-'; '-'; '--';'--';'--';'--'};
            lineWeight={.5;.5;.5;.5;.5;.5;.5;.5};
            lineColour={'r';'r';'r';'r';'b';'b';'b';'b'};
            lineMarker={'x';'.';'*';'o'};
            lineTypes={'-r';'-r';'-r';'--k';'--k';'--k';'--k'};
        else
            lineNames={'Pred a';'Pred b';'Pred ALT a';'Pred ALT b';'The Other Guy';'The Other Guy 2';'The Other Guy 3';'The Other Guy 4'};
            lineStyle={'-.'; '-'; '--'; '-.'};
            lineWeight={.5;.5;.5;.5;.5;.5;.5;.5};
            lineColour={'r';[0 0.7 0];'b';'k'};
            lineMarker={'x';'.';'*';'o'};
            lineTypes={'-r';'-m';'-g';'-b';'-m';'--b';'--r';'--g'};
        end
    end
    if plotStuff==1||printing==1
        fig1=figure();
    end
    title(titleText);
    plotStyleIndex=1;
    hold on
    ymin=min(averages-stdErr);
    ymin=min(ymin);
    ymax=max(averages+stdErr);
    ymax=max(ymax);
    
    %plot the preds output on a graph
    if plotStuff==1||printing==1
        sizeOfAverages=size(averages);
        currentTime=1;
        while currentTime<=numberOfTimes
            currentAgent=1;
            if sumAgents==1
                %plot the agents for a time in 1 colour
                lineName=sprintf('All agent Training Steps:%d',cloneTimes(currentTime));
                s1.a=cloneTimes(currentTime);
                s1.b=squeeze(averages(currentTime,:));
                s1.c=squeeze(stdErr(currentTime,:));
                outData(currentTime)=s1;
                [l,p]=boundedline(squeeze(1:sizeOfAverages(2)),squeeze(averages(currentTime,:)),squeeze(stdErr(currentTime,:)),cell2mat(lineTypes(plotStyleIndex)),'alpha','transparency', 0.01);%,'linestyle',cell2mat(lineStyle(plotStyleIndex,1)),'color',cell2mat(lineColour(plotStyleIndex,1)),'LineWidth',cell2mat(lineWeight(plotStyleIndex,1)),'DisplayName',file);
                if outlinesUsed==1
                    outline=outlinebounds(l,p);
                end
                %name main line for legend entry
                set(l,'DisplayName',lineName);
                %turn legend entries off for patch and edge lines
                if outlinesUsed==1
                    set(get(get(outline,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
                end
                set(get(get(p,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
            else
                while currentAgent<=numberOfAgents
                    %plot the agents for a time in 1 colour
                    lineName=sprintf('Agent %s Training Steps:%d',preditorNames(currentAgent),cloneTimes(currentTime));
                    [l,p]=boundedline(squeeze(1:sizeOfAverages(3)),squeeze(averages(currentTime,currentAgent,:)),squeeze(stdErr(currentTime,currentAgent,:)),cell2mat(lineTypes(plotStyleIndex)),'alpha','transparency', 0.01);%,'linestyle',cell2mat(lineStyle(plotStyleIndex,1)),'color',cell2mat(lineColour(plotStyleIndex,1)),'LineWidth',cell2mat(lineWeight(plotStyleIndex,1)),'DisplayName',file);
                    if outlinesUsed==1
                        outline=outlinebounds(l,p);
                    end
                    %name main line for legend entry
                    set(l,'DisplayName',lineName);
                    %turn legend entries off for patch and edge lines
                    if outlinesUsed==1
                        set(get(get(outline,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
                    end
                    set(get(get(p,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
                    currentAgent=currentAgent+1;
                end
            end
           
            plotStyleIndex=plotStyleIndex+1;
            if plotStyleIndex>length(lineTypes)
                plotStyleIndex=1;
            end
            currentTime=currentTime+1;
        end
        outDataName=sprintf('%soutData.mat',folder);
        save(outDataName,'outData');
        leg_end=legend(gca, 'show','Location','northwest');
        set(leg_end,'color','none');
    end
    %now make the plot a nice looking graph
end
end