function [ output_args ] = compareCloanPlots( input_args )
%COMPARECLOANPLOTS plot the data saved by catchCurves for cloaned
%   not much more to say
    function out = ThousandSep(in)
        %THOUSANDSEP adds thousands Separators to a 1x1 array.
        %   Example:
        %      ThousandSep(1234567)
        import java.text.*
        v = DecimalFormat;
        out = char(v.format(in));
    end
    function out = myTtest(mean1,mean2,var1,var2,numberOfExps)
        %myTtest a t test from mean and std dev        
        difMean=mean1-mean2;
        stdErr=sqrt(((var1^2)/numberOfExps)+((var2^2)/numberOfExps));
        dof=((var1^2/numberOfExps)+(var2^2/numberOfExps))^2;
        dof=dof/((var1^2/numberOfExps)^2/(numberOfExps-1)+(var2^2/numberOfExps)^2/(numberOfExps-1));
        t=difMean/stdErr;        
        out = tcdf(t,dof,'upper');        
    end
numberOfExperiments=200;
expName=   sprintf('PTL+conf4+third3speedShort+pr4v1+py2Line+10');
expNameAlt=sprintf('PTL+conf3+third3speedShort+pr4v1+py2Line+10');
folder=sprintf('C:\\Users\\Work-mine\\Documents\\WorkingDir\\PredPrey\\%s\\',expName);
folderAlt=sprintf('C:\\Users\\Work-mine\\Documents\\WorkingDir\\PredPrey\\%s\\',expNameAlt);
dontPlot=[1;2;3;4;5;6;7;8;9;10;11;12;13;14;15;16;17;18;19];%[2;3;4;5;7;8;9;10;12;13;14];%
%control bools
plotStuff=1;%0 or 1
printing=1;%0 or 1
publishing=1;
dataReductionFactor=20;
outlinesUsed=0;
xAxisLabelsType=1;%0 none 1 row 2 all
yAxisLabelsType=1;%0 none 1 row 2 all
xAxisTicks=1;
yAxisTicks=1;
axisXLabelText=sprintf('Time Steps');
axisYLabelText=sprintf('Preys Caught');
printTstats=1;
%load the saved .mat instead of reading csv
%prep data
loadDataName=sprintf('%soutData.mat',folderAlt);
load(loadDataName);
outDataAlt=outData;

loadDataName=sprintf('%soutData.mat',folder);
load(loadDataName);
%remove don't plot
numberNotPlotted=0;
for a=1:size(dontPlot)
    outData(dontPlot(a)-numberNotPlotted)=[];
    outDataAlt(dontPlot(a)-numberNotPlotted)=[];
    numberNotPlotted=numberNotPlotted+1;%adapted to removed
end
dataSize=size(outData);
numberOfDataItems=size(outData(1).b);
numberOfDataItems=numberOfDataItems(2);
dataCounter=1;
%prep figure
titleText=sprintf('Reused Data -Testing Catches: %s',expName);
if plotStuff==1||printing==1    
    fig1=figure('Name',titleText,'NumberTitle','off');
else
    annotation('textbox', [0 0.9 1 0.1], 'String', titleText,'EdgeColor', 'none', 'HorizontalAlignment', 'center')
end
plotStyleIndex=1;
hold on
numberOfElements=size(outData(1).b);%assume all the same size
numberOfElements=numberOfElements(2);
ymin=outData(1).b(1)-outData(1).c(1);%init the max and min
ymax=outData(1).b(1)+outData(1).c(1);
for currentLine=1:dataSize(2)%find min and max
    for currentElement=1:numberOfElements
        ymin=min(outData(currentLine).b(currentElement)-outData(currentLine).c(currentElement),ymin);
        ymax=max(outData(currentLine).b(currentElement)+outData(currentLine).c(currentElement),ymax);
    end
end
yminAlt=outDataAlt(1).b(1)-outDataAlt(1).c(1);%init the max and min
ymaxAlt=outData(1).b(1)+outDataAlt(1).c(1);
for currentLine=1:dataSize(2)%find min and max
    for currentElement=1:numberOfElements
        yminAlt=min(outDataAlt(currentLine).b(currentElement)-outDataAlt(currentLine).c(currentElement),yminAlt);
        ymaxAlt=max(outDataAlt(currentLine).b(currentElement)+outDataAlt(currentLine).c(currentElement),ymaxAlt);
    end
end
ymin=min(ymin,yminAlt);
ymax=max(ymax,ymaxAlt);
lineTypes={'-r';'-r';'-r';'-r';'-r';'-r'};
lineTypesAlt={'--k';'--k';'--k';'--k';'--k';'--k'};
%find subplor layout
previousRow=-1;
 legendSet=0;
N=dataSize(2);
K=1:N;
factors = K(rem(N,K)==0);
if 1==1+floor(sqrt(N))-ceil(sqrt(N))%is perfect square
    lowerIndex=factors(ceil(length(factors)/2));
    upperIndex=lowerIndex;
elseif mod(length(factors),2) == 0
    lowerIndex=factors(length(factors)/2);
    upperIndex=factors(length(factors)/2+1);
else
    lowerIndex=factors(floor(length(factors)/2));
    upperIndex=factors(ceil(length(factors)/2));
end
while dataCounter<=dataSize(2)
    subPlotHandle=subplot(lowerIndex,upperIndex,dataCounter);
    if yAxisLabelsType==0
        %nothing
    elseif yAxisLabelsType==1
        %rowbased
        if floor((dataCounter-1)/upperIndex)==floor(lowerIndex/2)&&floor((dataCounter-1)/upperIndex)>previousRow%1st only mid col2nd conditionevery row once%
            previousRow=floor((dataCounter-1)/upperIndex);
            ylabel(subPlotHandle,axisYLabelText);
        end
    elseif yAxisLabelsType==2
        %all
        ylabel(subPlotHandle,axisYLabelText);
    end
    if xAxisLabelsType==0
        %nothing
    elseif xAxisLabelsType==1
        %colbased
        if floor((dataCounter-1)/upperIndex)==(lowerIndex-1)&&(mod(dataCounter,(upperIndex)))==ceil(upperIndex/2)%1st only last row2nd conditionevery mid col%
            xlabel(subPlotHandle,axisXLabelText);
        end
    elseif xAxisLabelsType==2
        %all
        xlabel(subPlotHandle,axisXLabelText);
    end 
    if xAxisTicks==0
        set(gca,'XTickLabel',[])
    end
    if yAxisTicks==0
        set(gca,'YTickLabel',[])
    end
    if outData(dataCounter).a==1
        subPlotName=sprintf('%d Step',outData(dataCounter).a);
    else
        subPlotName=sprintf('%s Steps',ThousandSep(outData(dataCounter).a));
    end
    title(subPlotName,'FontWeight','Normal')
    xlim([0  numberOfDataItems])
    ylim([ymin ymax])
    %first line
    [l,p]=boundedline(squeeze(1:dataReductionFactor:numberOfDataItems),squeeze(outData(dataCounter).b(1:dataReductionFactor:numberOfElements)),squeeze(outData(dataCounter).c(1:dataReductionFactor:numberOfElements)),cell2mat(lineTypes(plotStyleIndex)),'alpha','transparency', 0.05);%,'linestyle',cell2mat(lineStyle(plotStyleIndex,1)),'color',cell2mat(lineColour(plotStyleIndex,1)),'LineWidth',cell2mat(lineWeight(plotStyleIndex,1)),'DisplayName',file);
    if outlinesUsed==1
        outline=outlinebounds(l,p);
    end
    probabilitiesOfBad=zeros(1,numberOfDataItems);
    if printTstats ==1
    for a=1:numberOfDataItems
    probabilitiesOfBad(1,a)=myTtest(outData(dataCounter).b(a),outDataAlt(dataCounter).b(a),outData(dataCounter).c(a),outDataAlt(dataCounter).c(a),numberOfExperiments);
    end
    minProbabilitiesOfBad(1,dataCounter)=min(probabilitiesOfBad);
    end
    %name main line for legend entry
    legName=strtok(expName,'+');
    lineName=sprintf('%s',legName(1,:));
    set(l,'DisplayName',lineName);
    %turn legend entries off for patch and edge lines
    if outlinesUsed==1
        set(get(get(outline,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
    end
    set(get(get(p,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
    %second line
    [l,p]=boundedline(squeeze(1:dataReductionFactor:numberOfDataItems),squeeze(outDataAlt(dataCounter).b(1:dataReductionFactor:numberOfElements)),squeeze(outDataAlt(dataCounter).c(1:dataReductionFactor:numberOfElements)),cell2mat(lineTypesAlt(plotStyleIndex)),'alpha','transparency', 0.05);%,'linestyle',cell2mat(lineStyle(plotStyleIndex,1)),'color',cell2mat(lineColour(plotStyleIndex,1)),'LineWidth',cell2mat(lineWeight(plotStyleIndex,1)),'DisplayName',file);
    if outlinesUsed==1
        outline=outlinebounds(l,p);
    end       
    %name main line for legend entry
    legName=strtok(expNameAlt,'+');
    lineName=sprintf('%s',legName(1,:));
    set(l,'DisplayName',lineName);
    %turn legend entries off for patch and edge lines
    if outlinesUsed==1
        set(get(get(outline,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
    end
    set(get(get(p,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
    plotStyleIndex=plotStyleIndex+1;
    if plotStyleIndex>length(lineTypes)
        plotStyleIndex=1;
    end
    if legendSet==0&&dataCounter==1
        leg_end=legend(gca, 'show','Location','northwest','Orientation','vertical');        
        set(leg_end,'color','none');
        legendSet=1;
    end
    dataCounter=dataCounter+1;
end
if printing==1
    if plotStuff==0
        set(fig1, 'visible','off');
    end
    outname=sprintf('%sCatchesSmall.tex',folder);
    outnamePDF=sprintf('%sCatchesSmall.pdf',folder);
    %set(gcf, 'color', 'none');
    %set(gca, 'color', 'none');
    %for tikz
    %notworking atemp at adding doc options autoly%matlab2tikz(outname,  'width', '\figurewidth','showInfo', false,'standalone', true);%show info false turns off a  message
    %removed font enc from matlab2tiokx
    matlab2tikz(outname,  'width', '\figurewidth','showInfo', false,'parseStringsAsMath',true,'standalone', true','extracode',['\pgfplotsset{','max space between ticks=20pt,','try min ticks=3,','yticklabel style={','/pgf/number format/fixed,','/pgf/number format/precision=1','},','scaled y ticks=false,','xticklabel style={','/pgf/number format/fixed,','/pgf/number format/precision=1','},','scaled x ticks=false,','title style={yshift=0pt,}','}']);%show info false turns off a  message
   %for pdf
%     paperX=12.5;%6.25
%     paperY=6.25;%7.5
%     set(gcf, 'PaperSize', [paperX paperY]);
%         set(gcf, 'PaperPositionMode', 'manual');
%         set(gcf, 'PaperPosition', [0 0 paperX paperY]);
%         set(gcf, 'PaperUnits', 'inches');
%         set(gcf, 'PaperSize', [paperX paperY]);
%         set(gcf, 'PaperPositionMode', 'manual');
%         set(gcf, 'PaperPosition', [0 0 paperX paperY]);
%         set(gca(), 'LooseInset', get(gca(), 'TightInset'));
%         if publishing==1
%             quality='-r2500';
%         else
%             quality='-r100';
%         end
    %print(gcf, '-dpdf','-noui',quality,outnamePDF)
end
    if printTstats ==1    
    minProbabilitiesOfBad
    end
end

